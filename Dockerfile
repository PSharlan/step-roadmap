FROM openjdk:11

ADD target/*.jar /app_step-roadmap.jar

ENTRYPOINT ["java", "-jar", "app_step-roadmap.jar"]