package by.itstep.steproadmap.service;

import by.itstep.steproadmap.dto.road.RoadCreateDto;
import by.itstep.steproadmap.dto.road.RoadFullDto;
import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import by.itstep.steproadmap.dto.road.RoadUpdateDto;
import java.util.List;

public interface RoadService {

    RoadFullDto findById(Integer id);

    List<RoadPreviewDto> findAll();

    List<RoadPreviewDto> findAllRoadsByRoadCategoryId(Integer id);

    RoadFullDto create(RoadCreateDto createDto);

    RoadFullDto update(RoadUpdateDto updateDto);

    void deleteById(Integer id);
}
