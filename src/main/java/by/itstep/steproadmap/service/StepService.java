package by.itstep.steproadmap.service;

import by.itstep.steproadmap.dto.step.StepCreateDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.dto.step.StepUpdateDto;
import java.util.List;

public interface StepService {

    StepFullDto findById(Integer id);

    List<StepFullDto> findAll();

    StepFullDto create(StepCreateDto createDto);

    StepFullDto update(StepUpdateDto updateDto);

    void deleteById(Integer id);
}
