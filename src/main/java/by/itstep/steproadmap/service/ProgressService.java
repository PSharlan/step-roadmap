package by.itstep.steproadmap.service;

import by.itstep.steproadmap.dto.progress.ProgressCreateDto;
import by.itstep.steproadmap.dto.progress.ProgressFullDto;
import by.itstep.steproadmap.dto.progress.ProgressUpdateDto;
import java.util.List;

public interface ProgressService {

    ProgressFullDto findById(Integer id);

    List<ProgressFullDto> findAll();

    List<ProgressFullDto> findAllProgressesByUserId(Integer id);

    ProgressFullDto create(ProgressCreateDto createDto);

    ProgressFullDto update(ProgressUpdateDto updateDto);

    void deleteById(Integer id);
}
