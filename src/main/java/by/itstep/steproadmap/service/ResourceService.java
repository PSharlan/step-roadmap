package by.itstep.steproadmap.service;


import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import by.itstep.steproadmap.dto.resource.ResourceUpdateDto;
import java.util.List;

public interface ResourceService {

    ResourceFullDto findById(Integer id);

    List<ResourceFullDto> findAll();

    ResourceFullDto create(ResourceCreateDto createDto);

    ResourceFullDto update(ResourceUpdateDto updateDto);

    void deleteById(Integer id);

}
