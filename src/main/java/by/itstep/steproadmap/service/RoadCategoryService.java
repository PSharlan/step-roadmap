package by.itstep.steproadmap.service;

import by.itstep.steproadmap.dto.category.RoadCategoryCreateDto;
import by.itstep.steproadmap.dto.category.RoadCategoryFullDto;
import by.itstep.steproadmap.dto.category.RoadCategoryUpdateDto;

import java.util.List;

public interface RoadCategoryService {

    RoadCategoryFullDto findById(Integer id);

    List<RoadCategoryFullDto> findAll();

    RoadCategoryFullDto create(RoadCategoryCreateDto createDto);

    RoadCategoryFullDto update(RoadCategoryUpdateDto updateDto);

    void deleteById(Integer id);
}
