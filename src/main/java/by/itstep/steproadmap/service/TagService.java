package by.itstep.steproadmap.service;

import by.itstep.steproadmap.dto.tag.TagCreateDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import by.itstep.steproadmap.dto.tag.TagUpdateDto;
import java.util.List;

public interface TagService {

    TagPreviewDto findById(Integer id);

    List<TagPreviewDto> findAll();

    TagPreviewDto create(TagCreateDto createDto);

    TagPreviewDto update(TagUpdateDto updateDto);

    void deleteById(Integer id);
}
