package by.itstep.steproadmap.service.impl;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import by.itstep.steproadmap.dto.resource.ResourceUpdateDto;
import by.itstep.steproadmap.entity.ResourceEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.exceptions.ResourceNotFoundException;
import by.itstep.steproadmap.exceptions.StepNotFoundException;
import by.itstep.steproadmap.repository.ResourceRepository;
import by.itstep.steproadmap.repository.StepRepository;
import by.itstep.steproadmap.service.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static by.itstep.steproadmap.mapper.ResourceMapper.RESOURCE_MAPPER;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private StepRepository stepRepository;

    @Override
    @Transactional(readOnly = true)
    public ResourceFullDto findById(Integer id) {
        ResourceEntity foundResource = resourceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id));

        log.info("ResourceServiceImpl -> Found resource {} by id {}", foundResource, id);
        return RESOURCE_MAPPER.mapToFullDto(foundResource);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResourceFullDto> findAll() {

        List<ResourceFullDto> resources = resourceRepository.findAll()
                .stream()
                .map(entity -> RESOURCE_MAPPER.mapToFullDto(entity))
                .collect(toList());

        log.info("ResourceServiceImpl -> Found resources {}", resources.size());
        return resources;
    }

    @Override
    @Transactional
    public ResourceFullDto create(ResourceCreateDto createDto) {
        ResourceEntity resourceToSave = RESOURCE_MAPPER.mapToEntity(createDto);

        StepEntity stepToMap = stepRepository.findById(createDto.getStepId())
                .orElseThrow(() -> new StepNotFoundException(createDto.getStepId()));

        resourceToSave.setStep(stepToMap);

        ResourceEntity savedEntity = resourceRepository.save(resourceToSave);

        log.info("ResourceServiceImpl -> Saved resource {}", savedEntity);
        return RESOURCE_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public ResourceFullDto update(ResourceUpdateDto updateDto) {
        ResourceEntity entityToUpdate = resourceRepository.findById(updateDto.getId())
                .orElseThrow(() -> new ResourceNotFoundException(updateDto.getId()));

        entityToUpdate.setResourceUrl(updateDto.getResourceUrl());
        entityToUpdate.setResourceType(updateDto.getResourceType());

        ResourceEntity updated = resourceRepository.save(entityToUpdate);

        log.info("ResourceServiceImpl -> Updated resource {}", updated);
        return RESOURCE_MAPPER.mapToFullDto(updated);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        resourceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id));

        resourceRepository.deleteById(id);
    }
}
