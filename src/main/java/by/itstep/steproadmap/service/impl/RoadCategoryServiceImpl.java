package by.itstep.steproadmap.service.impl;

import by.itstep.steproadmap.dto.category.RoadCategoryCreateDto;
import by.itstep.steproadmap.dto.category.RoadCategoryFullDto;
import by.itstep.steproadmap.dto.category.RoadCategoryUpdateDto;
import by.itstep.steproadmap.entity.RoadCategoryEntity;
import by.itstep.steproadmap.exceptions.RoadCategoryNotFoundException;
import by.itstep.steproadmap.exceptions.RoadNotFoundException;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.service.RoadCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import static by.itstep.steproadmap.mapper.RoadCategoryMapper.ROAD_CATEGORY_MAPPER;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class RoadCategoryServiceImpl implements RoadCategoryService {

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @Override
    @Transactional(readOnly = true)
    public RoadCategoryFullDto findById(Integer id) {
        RoadCategoryEntity foundRoadCategory = roadCategoryRepository.findById(id)
                .orElseThrow(() -> new RoadCategoryNotFoundException(id));

        log.info("RoadCategoryImpl -> Found road category {} by id: {} ", foundRoadCategory, id);
        return ROAD_CATEGORY_MAPPER.mapToFullDto(foundRoadCategory) ;
    }

    @Override
    @Transactional(readOnly = true)
    public List<RoadCategoryFullDto> findAll() {

        List<RoadCategoryFullDto> roadCategories = roadCategoryRepository.findAll()
                .stream()
                .map(entity -> ROAD_CATEGORY_MAPPER.mapToFullDto(entity))
                .collect(toList());

        log.info("RoadCategoryServiceImpl -> Found {} road categories ", roadCategories.size());
        return roadCategories;
    }

    @Override
    @Transactional
    public RoadCategoryFullDto create(RoadCategoryCreateDto createDto) {
        RoadCategoryEntity entityToSave = ROAD_CATEGORY_MAPPER.mapToEntity(createDto);

        RoadCategoryEntity savedEntity = roadCategoryRepository.save(entityToSave);

        log.info("RoadCategoryServiceImpl -> road category {} saved ", savedEntity);
        return ROAD_CATEGORY_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public RoadCategoryFullDto update(RoadCategoryUpdateDto updateDto) {
        RoadCategoryEntity entityToUpdate = roadCategoryRepository.findById(updateDto.getId())
                .orElseThrow(() -> new RoadNotFoundException(updateDto.getId()));

        entityToUpdate.setName(updateDto.getName());

        RoadCategoryEntity updated = roadCategoryRepository.save(entityToUpdate);

        log.info("RoadCategoryServiceImpl -> road category {} updated", updated);
        return ROAD_CATEGORY_MAPPER.mapToFullDto(updated);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        roadCategoryRepository.findById(id)
                .orElseThrow(() -> new RoadCategoryNotFoundException(id));

        roadCategoryRepository.deleteById(id);
    }
}
