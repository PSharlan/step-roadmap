package by.itstep.steproadmap.service.impl;

import by.itstep.steproadmap.dto.progress.ProgressCreateDto;
import by.itstep.steproadmap.dto.progress.ProgressFullDto;
import by.itstep.steproadmap.dto.progress.ProgressPreviewDto;
import by.itstep.steproadmap.dto.progress.ProgressUpdateDto;
import by.itstep.steproadmap.entity.ProgressEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.exceptions.ProgressNotFoundException;
import by.itstep.steproadmap.exceptions.RoadNotFoundException;
import by.itstep.steproadmap.exceptions.StepNotFoundException;
import by.itstep.steproadmap.repository.ProgressRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import by.itstep.steproadmap.service.ProgressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static by.itstep.steproadmap.mapper.ProgressMapper.PROGRESS_MAPPER;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class ProgressServiceImpl implements ProgressService {

    @Autowired
    private ProgressRepository progressRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private StepRepository stepRepository;

    @Override
    @Transactional(readOnly = true)
    public ProgressFullDto findById(Integer id) {
        ProgressEntity foundProgress = progressRepository.findById(id)
                .orElseThrow(() -> new ProgressNotFoundException(id));

        RoadEntity foundRoad = roadRepository.findById(foundProgress.getRoad().getId())
                .orElseThrow(() -> new RoadNotFoundException(foundProgress.getRoad().getId()));

        Integer quantityOfSteps = foundRoad.getSteps().size();

        Integer completenessPercent = (int) ((foundProgress.getCurrentStep().getPosition() / (double) quantityOfSteps) * 100);

        ProgressFullDto foundBeforeSettingPercent = PROGRESS_MAPPER.mapToFullDto(foundProgress);
        foundBeforeSettingPercent.setCompletenessPercent(completenessPercent);

        ProgressFullDto foundProgressWithCompletenessPercent = foundBeforeSettingPercent;

        log.info("ProgressServiceImpl -> Found progress {} by id: {} ", foundProgress, id);
        return foundProgressWithCompletenessPercent;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProgressFullDto> findAll() {

        List<ProgressFullDto> progresses = progressRepository.findAll()
                .stream()
                .map(entity -> PROGRESS_MAPPER.mapToFullDto(entity))
                .collect(toList());

        log.info("ProgressServiceImpl -> Found {} progress ", progresses.size());
        return progresses;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProgressFullDto> findAllProgressesByUserId(Integer userId) {

        List<ProgressEntity> progresses = progressRepository.findAllProgressesByUserId(userId);

        List<ProgressFullDto> foundProgressesWithCompletenessPercentList = new ArrayList<>();

        for (int i = 0; i < progresses.size(); i++) {

            Integer quantityOfSteps = progresses.get(i).getRoad().getSteps().size();

            Integer completenessPercent = (int) ((progresses.get(i).getCurrentStep().getPosition() / (double) quantityOfSteps) * 100);

            ProgressFullDto foundBeforeSettingPercent = PROGRESS_MAPPER.mapToFullDto(progresses.get(i));
            foundBeforeSettingPercent.setCompletenessPercent(completenessPercent);

            ProgressFullDto foundProgressWithCompletenessPercent = foundBeforeSettingPercent;

            foundProgressesWithCompletenessPercentList.add(foundProgressWithCompletenessPercent);
        }

        log.info("ProgressServiceImpl -> Found {} progress ", foundProgressesWithCompletenessPercentList.size());

        return foundProgressesWithCompletenessPercentList;
    }

    @Override
    @Transactional
    public ProgressFullDto create(ProgressCreateDto createDto) {
        ProgressEntity entityToSave = PROGRESS_MAPPER.mapToEntity(createDto);

        RoadEntity roadToMap = roadRepository.findById(createDto.getRoadId())
                .orElseThrow(() -> new RoadNotFoundException(createDto.getRoadId()));

        StepEntity stepToMap = stepRepository.findById(createDto.getStepId())
                .orElseThrow(() -> new StepNotFoundException(createDto.getStepId()));

        checkPosition(createDto);

        entityToSave.setRoad(roadToMap);
        entityToSave.setCurrentStep(stepToMap);
        entityToSave.setLastActiveTime(Instant.now());

        ProgressEntity savedEntity = progressRepository.save(entityToSave);

        log.info("ProgressServiceImpl -> progress {} saved", savedEntity);
        return PROGRESS_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public ProgressFullDto update(ProgressUpdateDto updateDto) {
        ProgressEntity entityToUpdate = progressRepository.findById(updateDto.getId())
                .orElseThrow(() -> new ProgressNotFoundException(updateDto.getId()));

        ProgressEntity entityAfterCheck = checkBelongToRoad(updateDto);

        entityToUpdate.setCurrentStep(entityAfterCheck.getCurrentStep());

        Integer quantityOfSteps = entityToUpdate.getRoad().getSteps().size();

        Integer completenessPercent = (int) ((entityToUpdate.getCurrentStep().getPosition() / (double) quantityOfSteps) * 100);

        ProgressFullDto progressToUpdateBeforeSettingPercent = PROGRESS_MAPPER.mapToFullDto(entityToUpdate);
        progressToUpdateBeforeSettingPercent.setCompletenessPercent(completenessPercent);

        ProgressFullDto updatedProgressWithCompletenessPercent = progressToUpdateBeforeSettingPercent;

        ProgressEntity updated = progressRepository.save(entityToUpdate);

        log.info("ProgressServiceImpl -> progress {} updated", updated);
        return updatedProgressWithCompletenessPercent;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        progressRepository.findById(id)
                .orElseThrow(() -> new ProgressNotFoundException(id));

        progressRepository.deleteById(id);
    }

    public void checkPosition(ProgressCreateDto createDto) {
        StepEntity stepToMap = stepRepository.findById(createDto.getStepId())
                .orElseThrow(() -> new StepNotFoundException(createDto.getStepId()));

        if (stepToMap.getPosition() != 1) {
            throw new RuntimeException(String.format("Step with id: %s is not at first position", createDto.getStepId()));
        }
    }

    public ProgressEntity checkBelongToRoad(ProgressUpdateDto updateDto) {

        ProgressEntity entityToUpdate = progressRepository.findById(updateDto.getId())
                .orElseThrow(() -> new ProgressNotFoundException(updateDto.getId()));

        RoadEntity roadEntity = roadRepository.findById(entityToUpdate.getRoad().getId())
                .orElseThrow(() -> new RoadNotFoundException(entityToUpdate.getRoad().getId()));

        StepEntity stepToMap = stepRepository.findById(updateDto.getCurrentStepId())
                .orElseThrow(() -> new StepNotFoundException(updateDto.getCurrentStepId()));

        if (!stepToMap.getRoad().getId().equals(roadEntity.getId())) {
            throw new RuntimeException(String.format("This step with id: %s does not belong to this road", updateDto.getCurrentStepId()));
        }
        return entityToUpdate;
    }
}
