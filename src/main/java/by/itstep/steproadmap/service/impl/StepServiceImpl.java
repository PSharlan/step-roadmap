package by.itstep.steproadmap.service.impl;

import by.itstep.steproadmap.dto.step.StepCreateDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.dto.step.StepUpdateDto;
import by.itstep.steproadmap.entity.ResourceEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.exceptions.RoadNotFoundException;
import by.itstep.steproadmap.exceptions.StepNotFoundException;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import by.itstep.steproadmap.service.StepService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static by.itstep.steproadmap.mapper.StepMapper.STEP_MAPPER;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class StepServiceImpl implements StepService {

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Override
    @Transactional(readOnly = true)
    public StepFullDto findById(Integer id) {
        StepEntity foundStep = stepRepository.findById(id)
                .orElseThrow(() -> new StepNotFoundException(id));

        log.info("StepServiceImpl -> Found Step {}", foundStep);
        return STEP_MAPPER.mapToFullDto(foundStep);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StepFullDto> findAll() {

        List<StepFullDto> steps = stepRepository.findAll()
                .stream()
                .map(entity -> STEP_MAPPER.mapToFullDto(entity))
                .collect(toList());

        log.info("StepServiceImpl -> Found {} steps ", steps);
        return steps;
    }

    @Override
    @Transactional
    public StepFullDto create(StepCreateDto createDto) {
        StepEntity entityToSave = STEP_MAPPER.mapToEntity(createDto);

        RoadEntity roadToMap = roadRepository.findById(createDto.getRoadId())
                .orElseThrow(() -> new RoadNotFoundException(createDto.getRoadId()));

        entityToSave.setRoad(roadToMap);

        if (createDto.getResources() == null) {
            throw new RuntimeException("Resources was not found");
        }
        for (ResourceEntity resources : entityToSave.getResources()) {
            resources.setStep(entityToSave);
        }

        StepEntity savedEntity = stepRepository.save(entityToSave);

        log.info("StepServiceImpl -> step {} saved", savedEntity);
        return STEP_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public StepFullDto update(StepUpdateDto updateDto) {
        StepEntity entityToUpdate = stepRepository.findById(updateDto.getId())
                .orElseThrow(() -> new StepNotFoundException(updateDto.getId()));

        entityToUpdate.setPosition(updateDto.getPosition());
        entityToUpdate.setDescription(updateDto.getDescription());
        entityToUpdate.setTitle(updateDto.getTitle());

        StepEntity updated = stepRepository.save(entityToUpdate);

        log.info("StepServiceImpl -> step {} updated", updated);
        return STEP_MAPPER.mapToFullDto(updated);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        stepRepository.findById(id)
                .orElseThrow(() -> new StepNotFoundException(id));

        stepRepository.deleteById(id);
    }
}
