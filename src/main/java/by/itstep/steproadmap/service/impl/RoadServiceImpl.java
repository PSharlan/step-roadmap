package by.itstep.steproadmap.service.impl;

import by.itstep.steproadmap.dto.road.RoadCreateDto;
import by.itstep.steproadmap.dto.road.RoadFullDto;
import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import by.itstep.steproadmap.dto.road.RoadUpdateDto;
import by.itstep.steproadmap.entity.RoadCategoryEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.TagEntity;
import by.itstep.steproadmap.exceptions.RoadNotFoundException;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.TagRepository;
import by.itstep.steproadmap.service.RoadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static by.itstep.steproadmap.mapper.RoadMapper.ROAD_MAPPER;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class RoadServiceImpl implements RoadService {

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @Override
    @Transactional(readOnly = true)
    public RoadFullDto findById(Integer id) {
        RoadEntity foundRoad = roadRepository.findById(id)
                .orElseThrow(() -> new RoadNotFoundException(id));

        log.info("RoadServiceImpl -> Found Road {}", foundRoad);
        return ROAD_MAPPER.mapToFullDto(foundRoad);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RoadPreviewDto> findAll() {

        List<RoadPreviewDto> roads = roadRepository.findAll()
                .stream()
                .map(entity -> ROAD_MAPPER.mapToPreviewDto(entity))
                .collect(toList());

        log.info("RoadServiceImpl -> Found Roads {}", roads.size());
        return roads;
    }

    @Override
    public List<RoadPreviewDto> findAllRoadsByRoadCategoryId(Integer id) {

        List<RoadPreviewDto> roads = roadRepository.findAllRoadsByRoadCategoryId(id)
                .stream()
                .map(entity -> ROAD_MAPPER.mapToPreviewDto(entity))
                .collect(toList());

        log.info("RoadServiseImpl -> Found Roads {} by category id: {}", roads.size(), id);

        return roads;
    }

    @Override
    @Transactional
    public RoadFullDto create(RoadCreateDto createDto) {
        RoadEntity entityToSave = ROAD_MAPPER.mapToEntity(createDto);

        RoadCategoryEntity entity = roadCategoryRepository.findById(createDto.getRoadCategoryId())
                .orElseThrow(()->new RoadNotFoundException(createDto.getRoadCategoryId()));

        List<TagEntity> tagToMap = tagRepository.findAllById(createDto.getTagIds());

        if (tagToMap.size() != createDto.getTagIds().size()) {
            throw new RuntimeException("Not all tags were found");
        }

        entityToSave.setTags(tagToMap);
        entityToSave.setRoadCategory(entity);

        System.out.println("RoadCategory in entity to save"+entityToSave.getRoadCategory());

        RoadEntity saveEntity = roadRepository.save(entityToSave);

        log.info("RoadServiceImpl -> Saved road {}", saveEntity);
        return ROAD_MAPPER.mapToFullDto(saveEntity);
    }

    @Override
    @Transactional
    public RoadFullDto update(RoadUpdateDto updateDto) {
        RoadEntity entityToUpdate = roadRepository.findById(updateDto.getId())
                .orElseThrow(() -> new RoadNotFoundException(updateDto.getId()));

        entityToUpdate.setTitle(updateDto.getTitle());
        entityToUpdate.setTitle(updateDto.getDescription());

        RoadEntity updated = roadRepository.save(entityToUpdate);

        log.info("RoadServiceImpl -> Updated road {}", updated);
        return ROAD_MAPPER.mapToFullDto(updated);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        roadRepository.findById(id)
                .orElseThrow(() -> new RoadNotFoundException(id));

        roadRepository.deleteById(id);
    }
}
