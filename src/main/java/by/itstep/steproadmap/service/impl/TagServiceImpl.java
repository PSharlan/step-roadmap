package by.itstep.steproadmap.service.impl;

import by.itstep.steproadmap.dto.tag.TagCreateDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import by.itstep.steproadmap.dto.tag.TagUpdateDto;
import by.itstep.steproadmap.entity.TagEntity;
import by.itstep.steproadmap.exceptions.TagNotFoundException;
import by.itstep.steproadmap.repository.TagRepository;
import by.itstep.steproadmap.service.TagService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static by.itstep.steproadmap.mapper.TagMapper.TAG_MAPPER;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    @Transactional(readOnly = true)
    public TagPreviewDto findById(Integer id) {
        TagEntity foundTag = tagRepository.findById(id)
                .orElseThrow(() -> new TagNotFoundException(id));

        log.info("TagServiceImpl -> Found tag {} by id {}", foundTag, id);
        return TAG_MAPPER.mapToPreviewDto(foundTag);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TagPreviewDto> findAll() {

        List<TagPreviewDto> tags = tagRepository.findAll()
                .stream()
                .map(entity -> TAG_MAPPER.mapToPreviewDto(entity))
                .collect(toList());

        log.info("TagServiceImpl -> Found {} tags ", tags.size());
        return tags;
    }

    @Override
    @Transactional
    public TagPreviewDto create(TagCreateDto createDto) {
        TagEntity tagToSave = TAG_MAPPER.mapToEntity(createDto);

        if ((tagRepository.findByName(createDto.getName()) != null)) {
            throw new RuntimeException(String.format("Tag with name: %s already exists", createDto.getName()));
        }
        TagEntity saved = tagRepository.save(tagToSave);

        log.info("TagServiceImpl -> Tag {} saved", saved);
        return TAG_MAPPER.mapToPreviewDto(saved);
    }

    @Override
    @Transactional
    public TagPreviewDto update(TagUpdateDto updateDto) {
        TagEntity tagToUpdate = tagRepository.findById(updateDto.getId())
                .orElseThrow(() -> new TagNotFoundException(updateDto.getId()));

        tagToUpdate.setName(updateDto.getName());

        TagEntity updated = tagRepository.save(tagToUpdate);

        log.info("TagServiceImpl -> Tag {} updated", updated);
        return TAG_MAPPER.mapToPreviewDto(updated);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        tagRepository.findById(id)
                .orElseThrow(() -> new TagNotFoundException(id));

        tagRepository.deleteById(id);
    }
}
