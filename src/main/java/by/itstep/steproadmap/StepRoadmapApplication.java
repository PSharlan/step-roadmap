package by.itstep.steproadmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepRoadmapApplication {

	public static void main(String[] args) {
		SpringApplication.run(StepRoadmapApplication.class, args);
	}

}
