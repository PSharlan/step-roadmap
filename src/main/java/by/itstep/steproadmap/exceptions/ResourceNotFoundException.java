package by.itstep.steproadmap.exceptions;

public class ResourceNotFoundException extends EntityNotFoundException {

    public ResourceNotFoundException(Integer id) {
        super("Resource ", id);
    }
}
