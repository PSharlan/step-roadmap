package by.itstep.steproadmap.exceptions;

public class StepNotFoundException extends EntityNotFoundException {

    public StepNotFoundException(Integer id) {
        super("Step ", id);
    }
}
