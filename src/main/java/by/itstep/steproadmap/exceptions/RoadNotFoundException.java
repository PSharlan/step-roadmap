package by.itstep.steproadmap.exceptions;

public class RoadNotFoundException extends EntityNotFoundException {

    public RoadNotFoundException(Integer id) {
        super("Road ", id);
    }
}
