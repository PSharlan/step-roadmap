package by.itstep.steproadmap.exceptions;


public class TagNotFoundException extends EntityNotFoundException {

    public TagNotFoundException(Integer id) {
        super("Tag ", id);
    }
}
