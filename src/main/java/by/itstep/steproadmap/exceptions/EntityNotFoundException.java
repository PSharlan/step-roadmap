package by.itstep.steproadmap.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String entity, Integer id) {
        super(entity + " was not found by id: " + id);
    }
}
