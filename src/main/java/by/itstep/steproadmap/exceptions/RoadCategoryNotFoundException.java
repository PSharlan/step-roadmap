package by.itstep.steproadmap.exceptions;

public class RoadCategoryNotFoundException extends EntityNotFoundException {

    public RoadCategoryNotFoundException(Integer id) {
        super("Road category ", id);
    }
}
