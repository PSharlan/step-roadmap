package by.itstep.steproadmap.exceptions;

public class ProgressNotFoundException extends EntityNotFoundException {

    public ProgressNotFoundException(Integer id){
        super("Progress ", id);
    }
}
