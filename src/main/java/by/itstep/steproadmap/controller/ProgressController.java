package by.itstep.steproadmap.controller;

import by.itstep.steproadmap.dto.progress.ProgressCreateDto;
import by.itstep.steproadmap.dto.progress.ProgressFullDto;
import by.itstep.steproadmap.dto.progress.ProgressPreviewDto;
import by.itstep.steproadmap.dto.progress.ProgressUpdateDto;
import by.itstep.steproadmap.service.ProgressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/progresses")
@Api("Controller dedicated to manage progress")
public class ProgressController {

    @Autowired
    private ProgressService progressService;

    @DeleteMapping("/all")
    public void clearAll() {
        // ... some code
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Search progress by id" , notes = "Existing id must be specified" )
    public ProgressFullDto getById(@PathVariable Integer id) {
        return progressService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all progresses")
    public List<ProgressFullDto> findAll() {
        return progressService.findAll();
    }

    @GetMapping("/user/{id}")
    @ApiOperation(value = "Search users all progresses by id", notes = "Existing id must be specified")
    public List<ProgressFullDto> findAllProgressesById(@PathVariable Integer id) {
        return progressService.findAllProgressesByUserId(id);
    }

    @PostMapping
    @ApiOperation(value = "Create new progress")
    public ResponseEntity<ProgressFullDto> create(@Valid @RequestBody ProgressCreateDto createDto) {
        return new ResponseEntity<>(progressService.create(createDto), HttpStatus.CREATED) ;
    }

    @PutMapping
    @ApiOperation(value = "Update existing progress")
    public ProgressFullDto update(@Valid @RequestBody ProgressUpdateDto updateDto) {
        return progressService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete progress by id", notes = "Existing id can not be null")
    public void deleteById(@PathVariable Integer id) {
        progressService.deleteById(id);
    }
}
