package by.itstep.steproadmap.controller;

import by.itstep.steproadmap.dto.tag.TagCreateDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import by.itstep.steproadmap.dto.tag.TagUpdateDto;
import by.itstep.steproadmap.service.TagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tags")
@Api("Controller dedicated to manage tags")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Search tag by id" , notes = "Existing id must be specified" )
    public TagPreviewDto getById(@PathVariable Integer id) {
        return tagService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all tags")
    public List<TagPreviewDto> findAll() {
        return tagService.findAll();
    }

    @PostMapping
    @ApiOperation(value = "Create new tag")
    public ResponseEntity<TagPreviewDto> create(@Valid @RequestBody TagCreateDto createDto) {
        return new ResponseEntity<>(tagService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update existing tag")
    public TagPreviewDto update(@Valid @RequestBody TagUpdateDto updateDto) {
        return tagService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete tag by id", notes = "Existing id can not be null")
    public void deleteById(@PathVariable Integer id) {
        tagService.deleteById(id);
    }
}
