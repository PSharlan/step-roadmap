package by.itstep.steproadmap.controller;

import by.itstep.steproadmap.dto.step.StepCreateDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.dto.step.StepUpdateDto;
import by.itstep.steproadmap.service.StepService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/steps")
@Api("Controller dedicated to manage steps on road")
public class StepController {

    @Autowired
    private StepService stepService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Search step by id" , notes = "Existing id must be specified" )
    public StepFullDto getById (@PathVariable Integer id){
        return stepService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all steps")
    public List<StepFullDto> findAll(){
        return stepService.findAll();
    }

    @PostMapping
    @ApiOperation(value = "Create new step")
    public ResponseEntity<StepFullDto> create(@Valid @RequestBody StepCreateDto createDto){
        return new ResponseEntity<>(stepService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update existing step")
    public StepFullDto update(@Valid @RequestBody StepUpdateDto updateDto){
        return stepService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete step by id", notes = "Existing id can not be null")
    public void deleteById(@PathVariable Integer id){
        stepService.deleteById(id);
    }
}
