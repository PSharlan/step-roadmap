package by.itstep.steproadmap.controller;


import by.itstep.steproadmap.dto.category.RoadCategoryCreateDto;
import by.itstep.steproadmap.dto.category.RoadCategoryFullDto;
import by.itstep.steproadmap.dto.category.RoadCategoryUpdateDto;
import by.itstep.steproadmap.service.RoadCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/road-category")
@Api("Controller dedicated to manage road categories")
public class RoadCategoryController {

    @Autowired
    private RoadCategoryService roadCategoryService;

    @GetMapping("{id}")
    @ApiOperation(value = "Search road category by id", notes = "Existing id must be specified")
    public RoadCategoryFullDto getById(@PathVariable Integer id) {
        return roadCategoryService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all road categories")
    public List<RoadCategoryFullDto> findAll() {
        return roadCategoryService.findAll();
    }

    @PostMapping
    @ApiOperation(value = "Create new road category")
    public ResponseEntity<RoadCategoryFullDto> create(@Valid @RequestBody RoadCategoryCreateDto createDto) {
        return new ResponseEntity<>(roadCategoryService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update existing road category")
    public RoadCategoryFullDto update(@Valid @RequestBody RoadCategoryUpdateDto updateDto){
        return roadCategoryService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete road category by id", notes = "Existing id can not be null")
    public void deleteById(@PathVariable Integer id){roadCategoryService.deleteById(id);}

}
