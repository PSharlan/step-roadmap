package by.itstep.steproadmap.controller;

import by.itstep.steproadmap.dto.road.RoadCreateDto;
import by.itstep.steproadmap.dto.road.RoadFullDto;
import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import by.itstep.steproadmap.dto.road.RoadUpdateDto;
import by.itstep.steproadmap.service.RoadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/roads")
@Api("Controller dedicated to manage roads")
public class RoadController {

    @Autowired
    private RoadService roadService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Search road by id" , notes = "Existing id must be specified" )
    public RoadFullDto getById(@PathVariable Integer id) {
        return roadService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all roads")
    public List<RoadPreviewDto> findAll() {
        return roadService.findAll();
    }

    @GetMapping("/category/{id}")
    @ApiOperation(value = "Find all road by road category id")
    public List<RoadPreviewDto> findAllRoadsByRoadCategoryId(@PathVariable Integer id){
        return roadService.findAllRoadsByRoadCategoryId(id);
    }

    @PostMapping
    @ApiOperation(value = "Create new road")
    public ResponseEntity<RoadFullDto> create(@Valid @RequestBody RoadCreateDto createDto) {
        return new ResponseEntity<>(roadService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update existing road")
    public RoadFullDto update(@Valid @RequestBody RoadUpdateDto updateDto) {
        return roadService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete road by id", notes = "Existing id can not be null")
    public void deleteById(@PathVariable Integer id) {
        roadService.deleteById(id);
    }
}
