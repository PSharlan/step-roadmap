package by.itstep.steproadmap.controller;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import by.itstep.steproadmap.dto.resource.ResourceUpdateDto;
import by.itstep.steproadmap.service.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/resources")
@Api("Controller dedicated to manage resources")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Search resource by id", notes = "Existing id must be specified")
    public ResourceFullDto getById(@PathVariable Integer id) {
        return resourceService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all resources")
    public List<ResourceFullDto> findAll() {
        return resourceService.findAll();
    }

    @PostMapping
    @ApiOperation(value = "Create new resource")
    public ResponseEntity<ResourceFullDto> create(@Valid @RequestBody ResourceCreateDto createDto) {
        return new ResponseEntity<>(resourceService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update existing resource")
    public ResourceFullDto update(@Valid @RequestBody ResourceUpdateDto updateDto) {
        return resourceService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete resource by id", notes = "Existing id can not be null")
    public void deleteById(@PathVariable Integer id) {
        resourceService.deleteById(id);
    }
}
