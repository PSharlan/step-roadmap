package by.itstep.steproadmap.repository;

import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RoadRepository extends JpaRepository<RoadEntity, Integer> {

    List<RoadEntity> findByTitle(String title);

    RoadEntity findOneById(Integer id);

    @Query("SELECT r FROM RoadEntity r JOIN r.tags t WHERE t.name = ?#{#tagParam.name}")
    List<RoadEntity> findAllByTag(@Param("tagParam")TagEntity tag);

    @Query(value = "SELECT * FROM road r WHERE r.road_category_id = :roadCategoryId", nativeQuery = true)
    List<RoadEntity> findAllRoadsByRoadCategoryId(@Param("roadCategoryId")Integer id);
}
