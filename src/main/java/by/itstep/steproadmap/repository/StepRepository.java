package by.itstep.steproadmap.repository;

import by.itstep.steproadmap.entity.StepEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StepRepository extends JpaRepository<StepEntity, Integer> {

    List<StepEntity> findByTitle(String title);

    StepEntity findOneById(Integer id);

}
