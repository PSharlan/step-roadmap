package by.itstep.steproadmap.repository;

import by.itstep.steproadmap.entity.RoadCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoadCategoryRepository extends JpaRepository<RoadCategoryEntity, Integer> {

    RoadCategoryEntity findOneById(Integer id);
}
