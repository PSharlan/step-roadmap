package by.itstep.steproadmap.repository;

import by.itstep.steproadmap.entity.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<TagEntity, Integer> {

    TagEntity findByName(String name);

    TagEntity findOneById(Integer id);

}
