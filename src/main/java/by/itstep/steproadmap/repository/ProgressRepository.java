package by.itstep.steproadmap.repository;

import by.itstep.steproadmap.entity.ProgressEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProgressRepository extends JpaRepository<ProgressEntity, Integer> {

    ProgressEntity findOneById(Integer id);

    @Query(value = "SELECT * FROM progress p WHERE p.user_id = :userId", nativeQuery = true)
    List<ProgressEntity> findAllProgressesByUserId(@Param("userId") Integer id);
}
