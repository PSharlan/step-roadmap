package by.itstep.steproadmap.repository;

import by.itstep.steproadmap.entity.ResourceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceRepository extends JpaRepository<ResourceEntity, Integer> {

    ResourceEntity findOneById(Integer id);

}
