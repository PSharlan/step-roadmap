package by.itstep.steproadmap.mapper;

import by.itstep.steproadmap.dto.category.RoadCategoryCreateDto;
import by.itstep.steproadmap.dto.category.RoadCategoryFullDto;
import by.itstep.steproadmap.entity.RoadCategoryEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {RoadMapper.class})
public interface RoadCategoryMapper {

    RoadCategoryMapper ROAD_CATEGORY_MAPPER = Mappers.getMapper(RoadCategoryMapper.class);

    RoadCategoryFullDto mapToFullDto(RoadCategoryEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "roads", ignore = true)
    RoadCategoryEntity mapToEntity(RoadCategoryCreateDto dto);

    List<RoadCategoryFullDto> mapToFullDtoList(List<RoadCategoryEntity> roadCategoryEntityList);
}
