package by.itstep.steproadmap.mapper;

import by.itstep.steproadmap.dto.progress.ProgressCreateDto;
import by.itstep.steproadmap.dto.progress.ProgressFullDto;
import by.itstep.steproadmap.entity.ProgressEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {StepMapper.class, RoadMapper.class})
public interface ProgressMapper {

    ProgressMapper PROGRESS_MAPPER = Mappers.getMapper(ProgressMapper.class);

    ProgressFullDto mapToFullDto(ProgressEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "road", ignore = true)
    @Mapping(target = "currentStep", ignore = true)
    @Mapping(target = "lastActiveTime", ignore = true)
    ProgressEntity mapToEntity(ProgressCreateDto dto);

    List<ProgressFullDto> mapToFullDtoList(List<ProgressEntity> progressEntityList);

}
