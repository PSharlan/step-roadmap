package by.itstep.steproadmap.mapper;

import by.itstep.steproadmap.dto.road.RoadCreateDto;
import by.itstep.steproadmap.dto.road.RoadFullDto;
import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import by.itstep.steproadmap.entity.RoadEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper(uses = {TagMapper.class, StepMapper.class})
public interface RoadMapper {

    RoadMapper ROAD_MAPPER = Mappers.getMapper(RoadMapper.class);

    RoadFullDto mapToFullDto(RoadEntity entity);

    RoadPreviewDto mapToPreviewDto(RoadEntity entity);

    List<RoadPreviewDto> mapToPreviewDto (List<RoadEntity> roadEntities);

    List<RoadFullDto> mapToFullDto (List<RoadEntity> roadEntityList);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "tags", ignore = true)
    RoadEntity mapToEntity(RoadCreateDto dto);
}
