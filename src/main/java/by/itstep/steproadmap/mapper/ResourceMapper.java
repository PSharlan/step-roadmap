package by.itstep.steproadmap.mapper;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import by.itstep.steproadmap.entity.ResourceEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ResourceMapper {

    ResourceMapper RESOURCE_MAPPER = Mappers.getMapper(ResourceMapper.class);

    @Mapping(target = "stepId", source = "step.id")
    ResourceFullDto mapToFullDto(ResourceEntity resourceEntity);

    List<ResourceFullDto> mapToFullDto(List<ResourceEntity> resourceEntityList);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "step", ignore = true)
    ResourceEntity mapToEntity(ResourceCreateDto dto);

}
