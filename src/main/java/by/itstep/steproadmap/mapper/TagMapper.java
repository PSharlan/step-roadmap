package by.itstep.steproadmap.mapper;

import by.itstep.steproadmap.dto.tag.TagCreateDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import by.itstep.steproadmap.entity.TagEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TagMapper {
    TagMapper TAG_MAPPER = Mappers.getMapper(TagMapper.class);

    TagPreviewDto mapToPreviewDto(TagEntity entity);

    List<TagPreviewDto> mapToPreviewDtoList(List<TagEntity> tagEntities);

    @Mapping(target = "roads", ignore = true)
    TagEntity mapToEntity(TagCreateDto dto);
}
