package by.itstep.steproadmap.mapper;

import by.itstep.steproadmap.dto.step.StepCreateDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.entity.StepEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = ResourceMapper.class)
public interface StepMapper {
    StepMapper STEP_MAPPER = Mappers.getMapper(StepMapper.class);

    @Mapping(target = "roadId", source = "road.id")
    StepFullDto mapToFullDto(StepEntity stepEntity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "road", ignore = true)
    StepEntity mapToEntity(StepCreateDto dto);

    List<StepFullDto> mapToFullDto (List<StepEntity> stepEntities);

}

