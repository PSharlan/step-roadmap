package by.itstep.steproadmap.entity.enums;

public enum ResourceType {
    VIDEO, ARTICLE, BOOK
}
