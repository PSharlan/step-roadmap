package by.itstep.steproadmap.entity;

import lombok.*;
import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "progress")
public class ProgressEntity {

    @Id
    @Column(name = "id", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "road_id", nullable = false)
    private RoadEntity road;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "current_step_id", nullable = false)
    private StepEntity currentStep;

    @Column(name = "last_active_time", nullable = false)
    private Instant lastActiveTime;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

}
