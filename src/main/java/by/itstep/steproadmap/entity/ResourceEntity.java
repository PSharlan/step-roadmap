package by.itstep.steproadmap.entity;

import by.itstep.steproadmap.entity.enums.ResourceType;
import lombok.*;
import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "resource")
public class ResourceEntity {

    @Id
    @Column(name = "id", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "step_id", nullable = false)
    private StepEntity step;

    @Column(name = "resource_url", nullable = true)
    private String resourceUrl;

    @Column(name = "type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private ResourceType resourceType;

}