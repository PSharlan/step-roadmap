package by.itstep.steproadmap.dto.road;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class RoadCreateDto {

    @ApiModelProperty(example ="Some title", notes = "Title must be specified")
    @NotBlank(message = "Title can not be null")
    private String title;

    @ApiModelProperty(example ="Some description", notes = "Description must be specified")
    @NotBlank(message = "Description can not be null")
    private String description;

    @ApiModelProperty(example ="1", notes = "List of tags ids")
    private List<Integer> tagIds;

    @ApiModelProperty(example = "1", notes = "Road category id must bt specified")
    private Integer roadCategoryId;

}
