package by.itstep.steproadmap.dto.road;

import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import lombok.Data;
import java.util.List;

@Data
public class RoadFullDto {

    private Integer id;
    private String title;
    private String description;
    private List<TagPreviewDto> tags;
    private List<StepFullDto> steps;
    private Integer roadCategoryId;
}
