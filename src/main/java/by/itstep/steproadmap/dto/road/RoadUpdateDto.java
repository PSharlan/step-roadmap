package by.itstep.steproadmap.dto.road;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RoadUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of existing road")
    @NotNull(message = "Id can not be null")
    private Integer id;

    @ApiModelProperty(example = "Some title", notes = "You can change title")
    @NotBlank(message = "Title can not be null")
    private String title;

    @ApiModelProperty(example = "Some description", notes = "You can change description ")
    @NotBlank(message = "Description can not be null")
    private String description;
}
