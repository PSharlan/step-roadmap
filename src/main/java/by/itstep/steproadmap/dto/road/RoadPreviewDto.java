package by.itstep.steproadmap.dto.road;

import lombok.Data;

@Data
public class RoadPreviewDto {

    private Integer id;
    private String title;
    private String description;
}
