package by.itstep.steproadmap.dto.step;

import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import lombok.Data;
import java.util.List;

@Data
public class StepFullDto {

    private Integer id;
    private String title;
    private String description;
    private Integer roadId;
    private Integer position;
    private List<ResourceFullDto> resources;
}
