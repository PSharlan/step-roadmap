package by.itstep.steproadmap.dto.step;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class StepCreateDto {

    @ApiModelProperty(example = "Some title", notes = "Title must be specified")
    @NotBlank(message = "Title can not be null")
    private String title;

    @ApiModelProperty(example = "Some description", notes = "Description must be specified")
    @NotBlank(message = "Description can not be null")
    private String description;

    @ApiModelProperty(example = "1", notes = "roadId must be specified")
    @NotNull(message = "roadId can not be null")
    private Integer roadId;

    @ApiModelProperty(example = "1", notes = "Position can not be null and must be #1")
    @NotNull(message = "Position can not be null")
    private Integer position;

    private List<ResourceCreateDto> resources;
}
