package by.itstep.steproadmap.dto.progress;

import by.itstep.steproadmap.dto.road.RoadFullDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import lombok.Data;
import java.time.Instant;

@Data
public class ProgressFullDto {

    private Integer id;
    private RoadFullDto road;
    private StepFullDto currentStep;
    private Instant lastActiveTime;
    private Integer userId;
    private Integer completenessPercent;
}
