package by.itstep.steproadmap.dto.progress;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
public class ProgressUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of existing progress")
    @NotNull(message = "Id can not be null")
    private Integer id;

    @ApiModelProperty(example = "1", notes = "currentStepId of existing step")
    @NotNull(message = "currentStepId can not be null")
    private Integer currentStepId;
}
