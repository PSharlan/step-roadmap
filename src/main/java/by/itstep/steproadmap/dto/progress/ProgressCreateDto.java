package by.itstep.steproadmap.dto.progress;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ProgressCreateDto {

    @ApiModelProperty(example = "1", notes = "roadId id must be specified")
    @NotNull(message = "roadId  can not be null")
    private Integer roadId;

    @ApiModelProperty(example = "1", notes = "stepId must be specified")
    @NotNull(message = "stepId can not be null")
    private Integer stepId;

    @ApiModelProperty(example = "1", notes = "userId must be specified" )
    @NotNull(message = "userId can not be null")
    private Integer userId;
}
