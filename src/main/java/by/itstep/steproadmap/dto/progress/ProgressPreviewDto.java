package by.itstep.steproadmap.dto.progress;

import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import by.itstep.steproadmap.dto.step.StepFullDto;

import java.time.Instant;

public class ProgressPreviewDto {
    private Integer id;
    private RoadPreviewDto road;
    private StepFullDto currentStep;
    private Instant lastActiveTime;
    private Integer userId;
    private Integer completenessPercent;
}
