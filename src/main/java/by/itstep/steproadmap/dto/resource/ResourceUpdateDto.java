package by.itstep.steproadmap.dto.resource;

import by.itstep.steproadmap.entity.enums.ResourceType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ResourceUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of existing progress")
    @NotNull(message = "Id can not be null")
    private Integer id;

    @ApiModelProperty(example = "www.example-resource.com", notes = "resourceUrl can be null")
    private String resourceUrl;

    @ApiModelProperty(example = "BOOK", notes = "Enum of existing resource types")
    @NotNull(message = "ResourceType can not be null")
    private ResourceType resourceType;
}
