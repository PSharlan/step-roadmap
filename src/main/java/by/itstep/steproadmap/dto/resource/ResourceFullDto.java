package by.itstep.steproadmap.dto.resource;

import by.itstep.steproadmap.entity.enums.ResourceType;
import lombok.Data;

@Data
public class ResourceFullDto {

    private Integer id;
    private Integer stepId;
    private String resourceUrl;
    private ResourceType resourceType;
}
