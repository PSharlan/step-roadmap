package by.itstep.steproadmap.dto.resource;

import by.itstep.steproadmap.entity.enums.ResourceType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ResourceCreateDto {

    @ApiModelProperty(example = "1", notes = "stepId must be specified")
    @NotNull(message = "Existing stepId can not be null")
    private Integer stepId;

    @ApiModelProperty(example = "www.example-resource.com", notes = "Can be empty")
    private String resourceUrl;

    @ApiModelProperty(example = "VIDEO", notes = "Resource type is Enum. Can not be null, All words -> UPPERCASE")
    @NotNull(message = "Resource type can not be null")
    private ResourceType resourceType;
}
