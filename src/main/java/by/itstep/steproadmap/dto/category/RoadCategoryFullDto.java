package by.itstep.steproadmap.dto.category;

import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import lombok.Data;

import java.util.List;

@Data
public class RoadCategoryFullDto {

    private Integer id;
    private String name;
    private List<RoadPreviewDto> roads;
}
