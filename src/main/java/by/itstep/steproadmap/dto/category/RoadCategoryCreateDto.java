package by.itstep.steproadmap.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RoadCategoryCreateDto {

    @ApiModelProperty(example = "Some name", notes = "Name must be specified")
    @NotBlank(message = "Name can not be null")
    private String name;
}
