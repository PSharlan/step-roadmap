package by.itstep.steproadmap.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RoadCategoryUpdateDto {

    @ApiModelProperty(example = "1", notes = " Id of existing road category")
    @NotNull(message = "Id can not be null")
    private Integer id;

    @ApiModelProperty(example = "Some name", notes = "Name can not be null")
    @NotBlank(message = "Name can not be null")
    private String name;
}
