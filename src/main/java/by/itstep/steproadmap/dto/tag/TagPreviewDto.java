package by.itstep.steproadmap.dto.tag;

import lombok.Data;

@Data
public class TagPreviewDto {

   private Integer id;
   private String name;
}
