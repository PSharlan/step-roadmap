package by.itstep.steproadmap.dto.tag;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TagUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of existing tag")
    @NotNull(message = "Id can not be null")
    private Integer id;

    @ApiModelProperty(example = "Some name", notes = "You can change the name")
    @NotBlank(message = "Name can not be null")
    private String name;
}
