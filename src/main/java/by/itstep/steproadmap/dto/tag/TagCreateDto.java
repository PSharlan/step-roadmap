package by.itstep.steproadmap.dto.tag;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TagCreateDto {

    @ApiModelProperty(example = "Tag name", notes = "Name must be specified")
    @NotBlank(message = "Name can not be null")
    private String name;
}
