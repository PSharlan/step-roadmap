package by.itstep.steproadmap.contollerTests;

import by.itstep.steproadmap.dto.tag.TagCreateDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import by.itstep.steproadmap.dto.tag.TagUpdateDto;
import by.itstep.steproadmap.entity.TagEntity;
import by.itstep.steproadmap.repository.TagRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.List;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TagControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TagRepository tagRepository;

    @BeforeEach
    void setUp() {
        tagRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        TagEntity savedTag = tagRepository.save(generateTag());

        //when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/tags/{id}", savedTag.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TagPreviewDto foundTag = objectMapper.readValue(bytes, TagPreviewDto.class);

        //then
        Assertions.assertNotNull(foundTag);
        Assertions.assertNotNull(foundTag.getId());
        Assertions.assertEquals(savedTag.getId(), foundTag.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        int notExistingId = 100000;

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/tags/{id}", notExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        tagRepository.saveAll(generateTags());

        //when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/tags"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<TagPreviewDto> foundTags = objectMapper.readValue(bytes, new TypeReference<List<TagPreviewDto>>() {
        });

        //then
        Assertions.assertNotNull(foundTags);
        Assertions.assertEquals(3, foundTags.size());
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        TagCreateDto createDto = generateTagCreateDto();

        //when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TagPreviewDto savedTag = objectMapper.readValue(bytes, TagPreviewDto.class);

        //then
        Assertions.assertNotNull(savedTag);
        Assertions.assertNotNull(savedTag.getId());
        Assertions.assertEquals(createDto.getName(),savedTag.getName());
    }

    @Test
    void create_whenNameIsEmpty() throws Exception {
        //given
        TagCreateDto createDto = generateTagCreateDto();
        createDto.setName(null);

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        TagEntity savedTag = tagRepository.save(generateTag());

        TagUpdateDto updateDto = generateTagUpdateDto();
        updateDto.setId(savedTag.getId());

        //then
        MvcResult result = mockMvc.perform(put("/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TagPreviewDto savedPreviewDto = objectMapper.readValue(bytes, TagPreviewDto.class);

        //then
        Assertions.assertNotNull(savedPreviewDto);
        Assertions.assertEquals(savedPreviewDto.getId(), savedTag.getId());
    }

    @Test
    void update_whenNameIsEmpty() throws Exception {
        //given
        TagEntity savedTag = tagRepository.save(generateTag());

        TagUpdateDto updateDto = generateTagUpdateDto();
        updateDto.setId(savedTag.getId());
        updateDto.setName(null);

        //then
        mockMvc.perform(put("/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenNotFound()  throws Exception{
        //given
        TagUpdateDto updateDto = generateTagUpdateDto();
        updateDto.setId(0);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/tags")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }
    @Test
    void deleteById_happyPath() throws Exception {
        //given
        TagEntity savedId = tagRepository.save(generateTag());

        //when
        mockMvc.perform(delete("/tags/{id}", savedId.getId()))
                .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/tags/{id}", savedId.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        int notExisting = 0;

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/tags/{id}", notExisting))
                .andExpect(status().isNotFound());
    }
}
