package by.itstep.steproadmap.contollerTests;


import by.itstep.steproadmap.dto.category.RoadCategoryCreateDto;
import by.itstep.steproadmap.dto.category.RoadCategoryFullDto;
import by.itstep.steproadmap.dto.category.RoadCategoryUpdateDto;
import by.itstep.steproadmap.entity.RoadCategoryEntity;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;
import static by.itstep.steproadmap.util.EntityGenerationUtils.generateRoadCategory;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class RoadCategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @Autowired
    private RoadRepository roadRepository;

    @BeforeEach
    void setUp() {
        roadRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());

        //when
        MvcResult result = mockMvc.perform(get("/road-category/{id}", savedRoadCategory.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        RoadCategoryFullDto foundRoadCategory = objectMapper.readValue(bytes, RoadCategoryFullDto.class);

        //then
        Assertions.assertNotNull(foundRoadCategory);
        Assertions.assertNotNull(foundRoadCategory.getId());
        Assertions.assertNotNull(foundRoadCategory.getName());
        Assertions.assertEquals(savedRoadCategory.getId(), foundRoadCategory.getId());
    }

    @Test
    void findById_whenNitFound() throws Exception {
        //given
        int notExistingId = 10000000;

        //when
        mockMvc.perform(get("/road-category/{id}", notExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll() throws Exception {
        //given
        roadCategoryRepository.save(generateRoadCategory());
        roadCategoryRepository.save(generateRoadCategory());

        //when
        MvcResult result = mockMvc.perform(get("/road-category"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<RoadCategoryFullDto> foundRoadCategory = objectMapper.readValue(bytes, new TypeReference<List<RoadCategoryFullDto>>() {
        });

        //then
        Assertions.assertNotNull(foundRoadCategory);
        Assertions.assertEquals(2, foundRoadCategory.size());
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        RoadCategoryCreateDto createDto = generateRoadCategoryCreateDto();

        //when
        MvcResult result = mockMvc.perform(post("/road-category")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        RoadCategoryFullDto savedRoadCategory = objectMapper.readValue(bytes, RoadCategoryFullDto.class);

        //then
        Assertions.assertNotNull(savedRoadCategory);
        Assertions.assertNotNull(savedRoadCategory.getId());
        Assertions.assertEquals(createDto.getName(), savedRoadCategory.getName());
    }

    @Test
    void create_whenNameIsEmpty() throws Exception {
        //given
        RoadCategoryCreateDto createDto = generateRoadCategoryCreateDto();
        createDto.setName(null);

        //when
        mockMvc.perform(post("/road-category")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());

        RoadCategoryUpdateDto updateDto = generateRoadCategoryUpdateDto();
        updateDto.setId(savedRoadCategory.getId());

        //when
        MvcResult result = mockMvc.perform(put("/road-category")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        RoadCategoryFullDto savedFullDto = objectMapper.readValue(bytes, RoadCategoryFullDto.class);

        //then
        Assertions.assertNotNull(savedFullDto);
        Assertions.assertEquals(savedFullDto.getId(), savedRoadCategory.getId());
    }

    @Test
    void update_whenNameIsNull() throws Exception {
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());

        RoadCategoryUpdateDto updateDto = generateRoadCategoryUpdateDto();
        updateDto.setId(savedRoadCategory.getId());
        updateDto.setName(null);

        //when
        MvcResult result = mockMvc.perform(put("/road-category")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());


        //when
        mockMvc.perform(delete("/road-category/{id}", savedRoadCategory.getId()))
                .andExpect(status().isOk());

        //then
        mockMvc.perform(get("/road-category/{id}", savedRoadCategory.getId()))
                .andExpect(status().isNotFound());
    }
    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        int notExisting = 0;

        //when
        mockMvc.perform(delete("/road-category/{id}", notExisting))
                .andExpect(status().isNotFound());
    }
}
