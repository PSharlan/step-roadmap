package by.itstep.steproadmap.contollerTests;

import by.itstep.steproadmap.dto.progress.ProgressCreateDto;
import by.itstep.steproadmap.dto.progress.ProgressFullDto;
import by.itstep.steproadmap.dto.progress.ProgressUpdateDto;
import by.itstep.steproadmap.entity.ProgressEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.repository.ProgressRepository;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ProgressControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProgressRepository progressRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        progressRepository.deleteAll();
        roadRepository.deleteAll();
        stepRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        ProgressEntity progressToSave = generateProgress();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToSave.setCurrentStep(savedStep);
        progressToSave.setRoad(savedRoad);

        ProgressEntity saved = progressRepository.save(progressToSave);

        //when
        MvcResult result = mockMvc.perform(get("/progresses/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ProgressFullDto foundProgress = objectMapper.readValue(bytes, ProgressFullDto.class);

        //then
        Assertions.assertNotNull(foundProgress);
        Assertions.assertNotNull(foundProgress.getId());
        Assertions.assertNotNull(foundProgress.getRoad());
        Assertions.assertEquals(saved.getId(), foundProgress.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        int notExistingId = 100000;

        //when
        mockMvc.perform(get("/progresses/{id}", notExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        progressRepository.saveAll(generateProgressList());

        //when
        MvcResult result = mockMvc.perform(get("/progresses"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<ProgressFullDto> foundProgress = objectMapper.readValue(bytes, new TypeReference<List<ProgressFullDto>>() {
        });

        //then
        Assertions.assertNotNull(foundProgress);
        Assertions.assertEquals(2, foundProgress.size());
    }

    @Test
    void findAllProgressesById() throws Exception{
        //given
        List<ProgressEntity> savedProgresses = progressRepository.saveAll(generateProgressList());

        //when
        MvcResult result = mockMvc.perform(get("/progresses/user/{id}", savedProgresses.get(0).getUserId() ))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<ProgressFullDto> foundProgress = objectMapper.readValue(bytes, new TypeReference<List<ProgressFullDto>>() {
        });

        //then
        Assertions.assertNotNull(foundProgress);
        Assertions.assertEquals(1,foundProgress.size());

    }

    @Test
    void create_happyPath() throws Exception {
        //given
        ProgressCreateDto createDto = generateProgressCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        createDto.setRoadId(savedRoad.getId());
        createDto.setStepId(savedStep.getId());

        //when
        MvcResult result = mockMvc.perform(post("/progresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ProgressFullDto savedProgress = objectMapper.readValue(bytes, ProgressFullDto.class);

        //then
        Assertions.assertNotNull(savedProgress);
        Assertions.assertNotNull(savedProgress.getId());
        Assertions.assertEquals(createDto.getRoadId(), savedProgress.getRoad().getId());
    }

    @Test
    void create_whenRoadIdIsEmpty() throws Exception {
        //given
        ProgressCreateDto createDto = generateProgressCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        createDto.setRoadId(null);
        createDto.setStepId(savedStep.getId());

        //when
        mockMvc.perform(post("/progresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenStepIdIsEmpty() throws Exception {
        //given
        ProgressCreateDto createDto = generateProgressCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        stepRepository.save(stepToSave);

        createDto.setRoadId(savedRoad.getId());
        createDto.setStepId(null);

        //when
        mockMvc.perform(post("/progresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenUserIdIsEmpty() throws Exception {
        //given
        ProgressCreateDto createDto = generateProgressCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        createDto.setRoadId(savedRoad.getId());
        createDto.setStepId(savedStep.getId());
        createDto.setUserId(null);

        //when
        mockMvc.perform(post("/progresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        ProgressEntity progressToSave = generateProgress();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToSave.setCurrentStep(savedStep);
        progressToSave.setRoad(savedRoad);

        ProgressEntity savedProgress = progressRepository.save(progressToSave);

        ProgressUpdateDto updateDto = generateProgressUpdateDto();
        updateDto.setId(savedProgress.getId());
        updateDto.setCurrentStepId(savedProgress.getCurrentStep().getId());

        //when
        MvcResult result = mockMvc.perform(put("/progresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ProgressFullDto savedPreviewDto = objectMapper.readValue(bytes, ProgressFullDto.class);

        //then
        Assertions.assertNotNull(savedPreviewDto);
        Assertions.assertEquals(savedPreviewDto.getId(), progressToSave.getId());
        Assertions.assertEquals(savedPreviewDto.getRoad().getId(), progressToSave.getRoad().getId());
    }

    @Test
    void update_whenStepIdIsEmpty() throws Exception{
        //given
        ProgressEntity progressToSave = generateProgress();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToSave.setCurrentStep(savedStep);
        progressToSave.setRoad(savedRoad);

        ProgressEntity savedProgress = progressRepository.save(progressToSave);

        ProgressUpdateDto updateDto = generateProgressUpdateDto();
        updateDto.setId(savedProgress.getId());
        updateDto.setCurrentStepId(null);

        //when
        mockMvc.perform(put("/progresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenNotFound() throws Exception {
        //given
        ProgressUpdateDto updateDto = generateProgressUpdateDto();
        updateDto.setId(0);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/progresses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        ProgressEntity progressToSave = generateProgress();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToSave.setCurrentStep(savedStep);
        progressToSave.setRoad(savedRoad);

        ProgressEntity saved = progressRepository.save(progressToSave);

        //when
        mockMvc.perform(delete("/progresses/{id}", saved.getId()))
                .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/progresses/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        int notExisting = 0;

        //when
        mockMvc.perform(delete("/progresses/{id}", notExisting))
                .andExpect(status().isNotFound());
    }

    public List<ProgressEntity> generateProgressList() {
        ProgressEntity progress1 = generateProgress();
        ProgressEntity progress2 = generateProgress();

        RoadEntity roadToSave1 = generateRoad();
        roadToSave1.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad1 = roadRepository.save(roadToSave1);

        RoadEntity roadToSave2 = generateRoad();
        roadToSave2.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad2 = roadRepository.save(roadToSave2);

        StepEntity stepToSet1 = generateStep();
        StepEntity stepToSet2 = generateStep();
        stepToSet1.setRoad(savedRoad1);
        stepToSet2.setRoad(savedRoad2);

        StepEntity savedStep1 = stepRepository.save(stepToSet1);
        StepEntity savedStep2 = stepRepository.save(stepToSet2);

        progress1.setRoad(savedRoad1);
        progress1.setCurrentStep(savedStep1);

        progress2.setRoad(savedRoad2);
        progress2.setCurrentStep(savedStep2);

        return Arrays.asList(progress1, progress2);
    }
}
