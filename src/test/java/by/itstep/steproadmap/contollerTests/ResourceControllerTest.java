package by.itstep.steproadmap.contollerTests;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import by.itstep.steproadmap.dto.resource.ResourceUpdateDto;
import by.itstep.steproadmap.entity.ResourceEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.repository.ResourceRepository;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateResourceCreateDto;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateResourceUpdateDto;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ResourceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        resourceRepository.deleteAll();
        roadRepository.deleteAll();
        stepRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        ResourceEntity resourceToSave = generateResource();

        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);

        ResourceEntity savedResource = resourceRepository.save(resourceToSave);

        //when
        MvcResult result = mockMvc.perform(get("/resources/{id}", savedResource.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ResourceFullDto foundResource = objectMapper.readValue(bytes, ResourceFullDto.class);

        //then
        Assertions.assertNotNull(foundResource);
        Assertions.assertNotNull(foundResource.getId());
        Assertions.assertNotNull(foundResource.getStepId());
        Assertions.assertEquals(savedResource.getId(), foundResource.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        int notExistingId = 100000;

        //when
        mockMvc.perform(get("/resources/{id}", notExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<ResourceEntity> savedResourceList = resourceRepository.saveAll(generateResourceList());

        //when
        MvcResult result = mockMvc.perform(get("/resources"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<ResourceFullDto> foundResource = objectMapper.readValue(bytes, new TypeReference<List<ResourceFullDto>>() {
        });

        //then
        Assertions.assertNotNull(foundResource);
        Assertions.assertEquals(2, foundResource.size());
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        ResourceCreateDto resourceCreateDto = generateResourceCreateDto();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceCreateDto.setStepId(savedStep.getId());

        //when
        MvcResult result = mockMvc.perform(post("/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(resourceCreateDto)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ResourceFullDto savedResource = objectMapper.readValue(bytes, ResourceFullDto.class);

        //then
        Assertions.assertNotNull(savedResource);
        Assertions.assertNotNull(savedResource.getId());
        Assertions.assertEquals(resourceCreateDto.getResourceType(), savedResource.getResourceType());
    }

    @Test
    void create_whenStepIdIsEmpty() throws Exception {
        //given
        ResourceCreateDto resourceCreateDto = generateResourceCreateDto();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        stepRepository.save(stepToSave);

        resourceCreateDto.setStepId(null);

        //when
        mockMvc.perform(post("/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(resourceCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenResourceTypeIsEmpty() throws Exception {
        //given
        ResourceCreateDto resourceCreateDto = generateResourceCreateDto();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceCreateDto.setStepId(savedStep.getId());
        resourceCreateDto.setResourceType(null);

        //when
        mockMvc.perform(post("/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(resourceCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        ResourceEntity resourceToSave = generateResource();

        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);

        ResourceEntity savedResource = resourceRepository.save(resourceToSave);

        ResourceUpdateDto updateDto = generateResourceUpdateDto();
        updateDto.setId(savedResource.getId());

        //when
        MvcResult result = mockMvc.perform(put("/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ResourceFullDto savedResourceDto = objectMapper.readValue(bytes, ResourceFullDto.class);

        //then
        Assertions.assertNotNull(savedResourceDto);
        Assertions.assertEquals(savedResource.getId(), savedResourceDto.getId());
        Assertions.assertEquals(savedResource.getStep().getId(), savedResourceDto.getStepId());
    }

    @Test
    void update_whenResourceTypeIsEmpty() throws Exception {
        //given
        ResourceEntity resourceToSave = generateResource();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);
        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);

        ResourceEntity savedResource = resourceRepository.save(resourceToSave);

        ResourceUpdateDto updateDto = generateResourceUpdateDto();
        updateDto.setId(savedResource.getId());
        updateDto.setResourceType(null);

        //when
        mockMvc.perform(put("/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenNotFound() throws Exception {
        //given
        ResourceUpdateDto updateDto = generateResourceUpdateDto();
        updateDto.setId(0);

        //when
        mockMvc.perform(put("/resources")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        ResourceEntity resourceToSave = generateResource();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);

        ResourceEntity savedResource = resourceRepository.save(resourceToSave);

        //when
        mockMvc.perform(delete("/resources/{id}", savedResource.getId()))
                .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/resources/{id}", savedResource.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteByID_whenNotFound() throws Exception {
        //given
        int notExisting = 100000;

        //when
        mockMvc.perform(delete("/resources/{id}", notExisting))
                .andExpect(status().isNotFound());
    }


    public List<ResourceEntity> generateResourceList() {
        ResourceEntity resourceEntity1 = generateResource();
        ResourceEntity resourceEntity2 = generateResource();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad1 = roadRepository.save(roadToSave);

        RoadEntity roadToSave2 = generateRoad();
        roadToSave2.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad2 = roadRepository.save(roadToSave2);

        StepEntity stepEntity1 = generateStep();
        StepEntity stepEntity2 = generateStep();

        stepEntity1.setRoad(savedRoad1);
        stepEntity2.setRoad(savedRoad2);

        StepEntity savedStep1 = stepRepository.save(stepEntity1);
        StepEntity savedStep2 = stepRepository.save(stepEntity2);

        resourceEntity1.setStep(savedStep1);
        resourceEntity2.setStep(savedStep2);

        return Arrays.asList(resourceEntity1, resourceEntity2);
    }
}
