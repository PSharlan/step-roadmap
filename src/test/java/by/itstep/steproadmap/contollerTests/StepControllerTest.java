package by.itstep.steproadmap.contollerTests;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.step.StepCreateDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.dto.step.StepUpdateDto;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.repository.ProgressRepository;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import java.util.List;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class StepControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private ProgressRepository progressRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        progressRepository.deleteAll();
        roadRepository.deleteAll();
        stepRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);

        //when
        MvcResult result = mockMvc.perform(get("/steps/{id}", stepToSave.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        StepFullDto foundStep = objectMapper.readValue(bytes, StepFullDto.class);

        //then
        Assertions.assertNotNull(foundStep);
        Assertions.assertNotNull(foundStep.getId());
        Assertions.assertEquals(savedStep.getId(), foundStep.getId());
        Assertions.assertEquals(savedStep.getTitle(), foundStep.getTitle());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        int notExisting = 10000;

        //when
        mockMvc.perform(get("/steps/{id}", notExisting))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<StepEntity> stepToSave = generateStepList();

        for (StepEntity entity : stepToSave) {
            RoadEntity roadToSave = generateRoad();
            roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
            RoadEntity savedRoad = roadRepository.save(roadToSave);
            entity.setRoad(savedRoad);
        }

        stepRepository.saveAll(stepToSave);

        //when
        MvcResult result = mockMvc.perform(get("/steps"))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<StepFullDto> foundSteps = objectMapper.readValue(bytes, new TypeReference<List<StepFullDto>>() {
        });

        //then
        Assertions.assertNotNull(foundSteps);
        Assertions.assertEquals(2, foundSteps.size());
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        StepCreateDto createDto = generateStepCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        List<ResourceCreateDto> resourceCreateDtoList = generateResourceCreateDtoList();

        createDto.setResources(resourceCreateDtoList);
        createDto.setRoadId(savedRoad.getId());

        //when
        MvcResult result = mockMvc.perform(post("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        StepFullDto savedStep = objectMapper.readValue(bytes, StepFullDto.class);

        // then
        Assertions.assertNotNull(savedStep);
        Assertions.assertNotNull(savedStep.getId());
        Assertions.assertEquals(createDto.getRoadId(), savedStep.getRoadId());
    }

    @Test
    void create_whenTitleIsEmpty() throws Exception {
        //given
        StepCreateDto createDto = generateStepCreateDto();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        List<ResourceCreateDto> resourceCreateDtoList = generateResourceCreateDtoList();

        createDto.setResources(resourceCreateDtoList);
        createDto.setRoadId(savedRoad.getId());
        createDto.setTitle(null);

        //when
        mockMvc.perform(post("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenDescriptionIsEmpty() throws Exception {
        //given
        StepCreateDto createDto = generateStepCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        List<ResourceCreateDto> resourceCreateDtoList = generateResourceCreateDtoList();

        createDto.setResources(resourceCreateDtoList);
        createDto.setRoadId(savedRoad.getId());
        createDto.setDescription(null);

        //when
        mockMvc.perform(post("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenRoadIdIsEmpty() throws Exception {
        //given
        StepCreateDto createDto = generateStepCreateDto();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        List<ResourceCreateDto> resourceCreateDtoList = generateResourceCreateDtoList();

        createDto.setResources(resourceCreateDtoList);
        createDto.setRoadId(null);

        //when
        mockMvc.perform(post("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenPositionIsEmpty() throws Exception {
        //given
        StepCreateDto createDto = generateStepCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        List<ResourceCreateDto> resourceCreateDtoList = generateResourceCreateDtoList();

        createDto.setResources(resourceCreateDtoList);
        createDto.setRoadId(savedRoad.getId());
        createDto.setPosition(null);

        //when
        mockMvc.perform(post("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);

        StepUpdateDto updateDto = generateStepUpdateDto();
        updateDto.setId(savedStep.getId());

        //when
        MvcResult result = mockMvc.perform(put("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        StepFullDto savedPreviewDto = objectMapper.readValue(bytes, StepFullDto.class);

        //then
        Assertions.assertNotNull(savedPreviewDto);
        Assertions.assertNotNull(savedPreviewDto.getId());
        Assertions.assertEquals(savedPreviewDto.getId(), savedStep.getId());
    }

    @Test
    void update_whenTitleIsEmpty() throws Exception {
        //given
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);

        StepUpdateDto updateDto = generateStepUpdateDto();
        updateDto.setId(savedStep.getId());
        updateDto.setTitle(null);

        //when
        mockMvc.perform(put("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenDescriptionIsEmpty() throws Exception {
        //given
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);

        StepUpdateDto updateDto = generateStepUpdateDto();
        updateDto.setId(savedStep.getId());
        updateDto.setDescription(null);

        //when
        mockMvc.perform(put("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenPositionIsEmpty() throws Exception {
        //given
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);

        StepUpdateDto updateDto = generateStepUpdateDto();
        updateDto.setId(savedStep.getId());
        updateDto.setPosition(null);

        //when
        mockMvc.perform(put("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenNotFound() throws Exception {
        //given
        StepUpdateDto updateDto = generateStepUpdateDto();
        updateDto.setId(0);

        //when
        mockMvc.perform(put("/steps")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void deleteById() throws Exception {
        //given
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);

        //when
        mockMvc.perform(delete("/steps/{id}", savedStep.getId()))
                .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/steps/{id}",savedStep.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        int notExisting = 0;

        //when
        mockMvc.perform(delete("/steps/{id}",notExisting))
                .andExpect(status().isNotFound());
    }
}
