package by.itstep.steproadmap.contollerTests;

import by.itstep.steproadmap.dto.road.RoadCreateDto;
import by.itstep.steproadmap.dto.road.RoadFullDto;
import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import by.itstep.steproadmap.dto.road.RoadUpdateDto;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.TagEntity;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.TagRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import liquibase.pro.packaged.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateRoadCreateDto;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateRoadUpdateDto;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class RoadControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        roadRepository.deleteAll();
        tagRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        //when
        MvcResult result = mockMvc.perform(get("/roads/{id}", savedRoad.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        RoadFullDto foundRoad = objectMapper.readValue(bytes, RoadFullDto.class);

        //then
        Assertions.assertNotNull(foundRoad);
        Assertions.assertNotNull(foundRoad.getId());
        Assertions.assertNotNull(foundRoad.getDescription());
        Assertions.assertEquals(savedRoad.getId(), foundRoad.getId());
        Assertions.assertEquals(savedRoad.getTitle(), foundRoad.getTitle());
        Assertions.assertEquals(savedRoad.getTitle(), foundRoad.getTitle());
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<RoadEntity> roadToSave = generateRoadList();

        for (RoadEntity road : roadToSave){
            road.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        }
        roadRepository.saveAll(roadToSave);

        //when
        MvcResult result = mockMvc.perform(get("/roads"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<RoadPreviewDto> foundRoad = objectMapper.readValue(bytes, new TypeReference<List<RoadPreviewDto>>() {
        });

        //then
        Assertions.assertNotNull(foundRoad);
        Assertions.assertEquals(2, foundRoad.size());
    }

    @Test
    void findAllRoadsByCategoryId() throws Exception{
        //given
        List<RoadEntity> roadToSave = generateRoadList();

        for (RoadEntity road : roadToSave){
            road.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        }
        List<RoadEntity> savedRoads = roadRepository.saveAll(roadToSave);

        //when
        MvcResult result = mockMvc.perform(get("/roads/category/{id}", savedRoads.get(0).getRoadCategory().getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<RoadPreviewDto> foundRoad = objectMapper.readValue(bytes, new TypeReference<List<RoadPreviewDto>>() {
        });

        //then
        Assertions.assertNotNull(foundRoad);
        Assertions.assertEquals(1,foundRoad.size());
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        RoadCreateDto createDto = generateRoadCreateDto();

        List<TagEntity> tagEntityList = generateTags();
        List<Integer> tagsId = new ArrayList<>();

        for (TagEntity entity : tagEntityList) {
            TagEntity savedTag = tagRepository.save(entity);
            tagsId.add(savedTag.getId());
        }
        createDto.setRoadCategoryId(roadCategoryRepository.save(generateRoadCategory()).getId());
        createDto.setTagIds(tagsId);

        //when
        MvcResult result = mockMvc.perform(post("/roads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        RoadFullDto savedRoad = objectMapper.readValue(bytes, RoadFullDto.class);

        //then
        Assertions.assertNotNull(savedRoad);
        Assertions.assertNotNull(savedRoad.getId());
        Assertions.assertEquals(createDto.getTitle(), savedRoad.getTitle());
        Assertions.assertEquals(createDto.getDescription(), savedRoad.getDescription());
    }

    @Test
    void create_whenTitleIsEmpty() throws Exception {
        //given
        RoadCreateDto createDto = generateRoadCreateDto();

        List<TagEntity> tagEntityList = generateTags();
        List<Integer> tagsId = new ArrayList<>();

        for (TagEntity entity : tagEntityList) {
            TagEntity savedTag = tagRepository.save(entity);
            tagsId.add(savedTag.getId());
        }
        createDto.setTagIds(tagsId);
        createDto.setTitle(null);

        //when
        mockMvc.perform(post("/roads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenDescriptionIsEmpty() throws Exception {
        //given
        RoadCreateDto createDto = generateRoadCreateDto();

        List<TagEntity> tagEntityList = generateTags();
        List<Integer> tagsId = new ArrayList<>();

        for (TagEntity entity : tagEntityList) {
            TagEntity savedTag = tagRepository.save(entity);
            tagsId.add(savedTag.getId());
        }
        createDto.setTagIds(tagsId);
        createDto.setDescription(null);

        //when
        mockMvc.perform(post("/roads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        RoadUpdateDto updateDto = generateRoadUpdateDto();
        updateDto.setId(savedRoad.getId());

        //when
        MvcResult result = mockMvc.perform(put("/roads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        RoadFullDto savedFullDto = objectMapper.readValue(bytes, RoadFullDto.class);

        //then
        Assertions.assertNotNull(savedFullDto);
        Assertions.assertNotNull(savedFullDto.getId());
    }

    @Test
    void update_whenTitleIsEmpty() throws Exception {
        //given
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        RoadUpdateDto updateDto = generateRoadUpdateDto();
        updateDto.setId(savedRoad.getId());
        updateDto.setTitle(null);

        //when
        mockMvc.perform(put("/roads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenDescriptionIsEmpty() throws Exception {
        //given
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        RoadUpdateDto updateDto = generateRoadUpdateDto();
        updateDto.setId(savedRoad.getId());
        updateDto.setDescription(null);

        //when
        mockMvc.perform(put("/roads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void upgrade_whenNotFound() throws Exception {
        //given
        RoadUpdateDto updateDto = generateRoadUpdateDto();
        updateDto.setId(0);

        //when
        mockMvc.perform(put("/roads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        //when
        mockMvc.perform(delete("/roads/{id}", savedRoad.getId()))
                .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/roads/{id}", savedRoad.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        int notExistingId = 0;

        //when
        mockMvc.perform(delete("/roads/{id}", notExistingId))
                .andExpect(status().isNotFound());
    }
}
