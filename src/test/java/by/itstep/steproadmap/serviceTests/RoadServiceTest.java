package by.itstep.steproadmap.serviceTests;

import by.itstep.steproadmap.dto.road.RoadCreateDto;
import by.itstep.steproadmap.dto.road.RoadFullDto;
import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import by.itstep.steproadmap.dto.road.RoadUpdateDto;
import by.itstep.steproadmap.entity.*;
import by.itstep.steproadmap.exceptions.RoadNotFoundException;
import by.itstep.steproadmap.repository.*;
import by.itstep.steproadmap.service.RoadService;
import ch.qos.logback.core.net.AbstractSSLSocketAppender;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class RoadServiceTest {
    @Autowired
    private RoadService roadService;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private ProgressRepository progressRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;


    @BeforeEach
    void setUp() {
        progressRepository.deleteAll();
        roadRepository.deleteAll();
        stepRepository.deleteAll();
        tagRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));

        RoadEntity savedResource = roadRepository.save(roadToSave);

        //when
        RoadFullDto foundResource = roadService.findById(savedResource.getId());

        //then
        Assertions.assertNotNull(foundResource);
        Assertions.assertEquals(foundResource.getId(), savedResource.getId());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<RoadEntity> toSave = generateRoadList();

        for (RoadEntity entity : toSave) {
            entity.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        }

        List<RoadEntity> saved = roadRepository.saveAll(toSave);

        //when
        List<RoadPreviewDto> foundResources = roadService.findAll();

        //then
        Assertions.assertNotNull(foundResources);
        Assertions.assertEquals(saved.size(), foundResources.size());
        Assertions.assertEquals(2, foundResources.size());
    }

    @Test
    void findAllRoadsByCategoryId() {
        //given
        List<RoadEntity> toSave = generateRoadList();

        for (RoadEntity entity : toSave) {
            entity.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        }

        List<RoadEntity> saved = roadRepository.saveAll(toSave);

        //when
        List<RoadPreviewDto> foundRoads = roadService.findAllRoadsByRoadCategoryId(saved.get(0).getRoadCategory().getId());

        //then
        Assertions.assertNotNull(foundRoads);
        Assertions.assertEquals(1, foundRoads.size());

    }

    @Test
    void create_happyPath() {
        //given
        RoadCreateDto roadToSave = generateRoadCreateDto();
        List<TagEntity> tagEntityList = generateTags();
        List<Integer> tagsId = new ArrayList<>();

        for (TagEntity entity : tagEntityList) {
            TagEntity savedTag = tagRepository.save(entity);
            tagsId.add(savedTag.getId());
        }

        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());
        roadToSave.setRoadCategoryId(savedRoadCategory.getId());

        roadToSave.setTagIds(tagsId);

        //when
        RoadFullDto saved = roadService.create(roadToSave);
        RoadFullDto foundSavedResource = roadService.findById(saved.getId());

        //then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(foundSavedResource);
        Assertions.assertEquals(saved.getId(), foundSavedResource.getId());
    }

    @Test
    void update_happyPath() {
        //given
        RoadCreateDto roadToCreate = generateRoadCreateDto();

        List<TagEntity> tagEntityList = generateTags();
        List<Integer> tagsId = new ArrayList<>();

        for (TagEntity entity : tagEntityList) {
            TagEntity savedTag = tagRepository.save(entity);
            tagsId.add(savedTag.getId());
        }
        roadToCreate.setRoadCategoryId(roadCategoryRepository.save(generateRoadCategory()).getId());
        roadToCreate.setTagIds(tagsId);

        RoadFullDto savedRoad = roadService.create(roadToCreate);

        RoadUpdateDto updateDto = generateRoadUpdateDto();
        updateDto.setId(savedRoad.getId());
        updateDto.setTitle("Bingo");

        //when
        RoadFullDto updated = roadService.update(updateDto);

        //then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(savedRoad.getId(), updated.getId());
        Assertions.assertEquals(updated.getDescription(), savedRoad.getDescription());
        Assertions.assertNotEquals(updated.getTitle(), savedRoad.getTitle());
    }

    @Test
    void deleteById_happyPath() {
        //given
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        Integer idForDelete = savedRoad.getId();

        //when
        roadService.deleteById(idForDelete);

        Exception exception = assertThrows(RoadNotFoundException.class,
                () -> roadService.findById(idForDelete));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(idForDelete)));
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistingId = 10000;

        //when
        Exception exception = assertThrows(RoadNotFoundException.class,
                ()-> roadService.findById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound(){
        //given
        RoadUpdateDto updateDto = generateRoadUpdateDto();

        Integer notExistingId = 10000;
        updateDto.setId(notExistingId);

        //when
        Exception exception = assertThrows(RoadNotFoundException.class,
                () -> roadService.update(updateDto));

        //then
        Assertions.assertTrue((exception.getMessage().contains(String.valueOf(notExistingId))));
    }

    @Test
    void delete_whenNotFound(){
        //given
        Integer notExistingId = 10000;

        //when
         Exception exception = assertThrows(RoadNotFoundException.class,
                 () -> roadService.deleteById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }
}