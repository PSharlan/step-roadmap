package by.itstep.steproadmap.serviceTests;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import by.itstep.steproadmap.dto.resource.ResourceUpdateDto;
import by.itstep.steproadmap.entity.ResourceEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.exceptions.ResourceNotFoundException;
import by.itstep.steproadmap.repository.ResourceRepository;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import by.itstep.steproadmap.service.ResourceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ResourceServiceTest {
    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        resourceRepository.deleteAll();
        roadRepository.deleteAll();
        stepRepository.deleteAll();
        roadRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        ResourceEntity resourceToSave = generateResource();
        resourceToSave.setStep(savedStep);

        ResourceEntity savedResource = resourceRepository.save(resourceToSave);

        //when
        ResourceFullDto foundResource = resourceService.findById(savedResource.getId());

        //then
        Assertions.assertNotNull(foundResource);
        Assertions.assertEquals(foundResource.getId(), savedResource.getId());

    }

    @Test
    void findAll_happyPath() {
        //given
        List<ResourceEntity> toSave = generateResourceList();

        List<ResourceEntity> saved = resourceRepository.saveAll(toSave);

        //when
        List<ResourceFullDto> foundResources = resourceService.findAll();

        //then
        Assertions.assertNotNull(foundResources);
        Assertions.assertEquals(saved.size(), foundResources.size());
        Assertions.assertEquals(2, foundResources.size());
    }

    @Test
    void create_happyPath() {
        //given
        ResourceCreateDto resourceToSave = generateResourceCreateDto();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);
        resourceToSave.setStepId(savedStep.getId());

        //when
        ResourceFullDto saved = resourceService.create(resourceToSave);
        ResourceFullDto foundSavedResource = resourceService.findById(saved.getId());

        //then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(foundSavedResource);
        Assertions.assertEquals(saved.getId(), foundSavedResource.getId());
    }

    @Test
    void update_happyPath() {
        //given
        ResourceCreateDto resourceToUpdate = generateResourceCreateDto();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();
        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);
        resourceToUpdate.setStepId(savedStep.getId());

        ResourceFullDto savedResource = resourceService.create(resourceToUpdate);

        ResourceUpdateDto updateDto = generateResourceUpdateDto();
        updateDto.setId(savedResource.getId());

        //when
        ResourceFullDto updated = resourceService.update(updateDto);

        //then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(savedResource.getId(), updated.getId());
        Assertions.assertEquals(savedResource.getStepId(), updated.getStepId());
    }

    @Test
    void deleteById_happyPath() {
        //given
        ResourceEntity resourceToSave = generateResource();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();
        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);

        ResourceEntity saved = resourceRepository.save(resourceToSave);
        Integer idForDelete = saved.getId();

        //when
        resourceService.deleteById(idForDelete);

        Exception exception = assertThrows(ResourceNotFoundException.class,
                () -> resourceService.findById(saved.getId()));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(saved.getId())));
    }

    @Test
    void findById_whenNotFind() {
        //given
        Integer notExistingId = 10000;

        //when
        Exception exception = assertThrows(ResourceNotFoundException.class,
                () -> resourceService.findById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound() {
        //given
        ResourceUpdateDto updateDto = generateResourceUpdateDto();

        Integer notExisting = 10000;
        updateDto.setId(notExisting);

        //when
        Exception exception = assertThrows(ResourceNotFoundException.class,
                () -> resourceService.update(updateDto));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
    }

    @Test
    void deleteById_whenNotFound() {
        //given
        Integer notExisting = 10000;

        //when
        Exception exception = assertThrows(ResourceNotFoundException.class,
                () -> resourceService.deleteById(notExisting));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
    }


    public List<ResourceEntity> generateResourceList() {
        ResourceEntity resourceEntity1 = generateResource();
        ResourceEntity resourceEntity2 = generateResource();

        RoadEntity road1 = generateRoad();
        road1.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(road1);

        RoadEntity road2 =generateRoad();
        road2.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad2 = roadRepository.save(road2);

        StepEntity stepEntity1 = generateStep();
        StepEntity stepEntity2 = generateStep();

        stepEntity1.setRoad(savedRoad);
        stepEntity2.setRoad(savedRoad2);

        StepEntity savedStep1 = stepRepository.save(stepEntity1);
        StepEntity savedStep2 = stepRepository.save(stepEntity2);

        resourceEntity1.setStep(savedStep1);
        resourceEntity2.setStep(savedStep2);

        return Arrays.asList(resourceEntity1, resourceEntity2);
    }
}
