package by.itstep.steproadmap.serviceTests;

import by.itstep.steproadmap.dto.tag.TagCreateDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import by.itstep.steproadmap.dto.tag.TagUpdateDto;
import by.itstep.steproadmap.entity.TagEntity;
import by.itstep.steproadmap.exceptions.TagNotFoundException;
import by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils;
import by.itstep.steproadmap.repository.TagRepository;
import by.itstep.steproadmap.service.TagService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateTagUpdateDto;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class TagServiceTest {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private TagService tagService;

    @BeforeEach
    void setUp() {
        tagRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        TagEntity saved = tagRepository.save(generateTag());

        //when
        TagPreviewDto foundTag = tagService.findById(saved.getId());

        //then
        Assertions.assertNotNull(foundTag);
        Assertions.assertEquals(saved.getId(), foundTag.getId());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<TagEntity> saved = tagRepository.saveAll(generateTags());

        //when
        List<TagPreviewDto> foundTags = tagService.findAll();

        //then
        Assertions.assertNotNull(foundTags);
        Assertions.assertEquals(saved.size(), foundTags.size());
    }

    @Test
    void create_happyPath() {
        //given
        TagCreateDto tagToSave = EntityMapperGenerationUtils.generateTagCreateDto();

        //when
        TagPreviewDto saved = tagService.create(tagToSave);
        TagPreviewDto foundSavedTag = tagService.findById(saved.getId());

        //then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(foundSavedTag);
        Assertions.assertEquals(saved.getId(), foundSavedTag.getId());
    }

    @Test
    void update_happyPath() {
        //given
        TagCreateDto tagToCreate = EntityMapperGenerationUtils.generateTagCreateDto();

        TagPreviewDto savedTag = tagService.create(tagToCreate);

        TagUpdateDto updateDto = generateTagUpdateDto();
        updateDto.setId(savedTag.getId());

        //when
        TagPreviewDto updated = tagService.update(updateDto);

        //then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(savedTag.getId(), updated.getId());
        Assertions.assertNotEquals(savedTag.getName(), updated.getName());
    }

    @Test
    void deleteById_happyPath() {
        //given
        TagEntity tagToSave = generateTag();

        TagEntity saved = tagRepository.save(tagToSave);
        Integer idForDelete = saved.getId();

        //when
        tagService.deleteById(idForDelete);

        Exception exception = assertThrows(TagNotFoundException.class,
                () -> tagService.findById(saved.getId()));

        //then
        Assertions.assertNull(tagRepository.findOneById(idForDelete));
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistingId = 10000;

        //when
        Exception exception =assertThrows(TagNotFoundException.class,
                () -> tagService.findById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound(){
        //given
        TagUpdateDto updateDto = generateTagUpdateDto();

        Integer notExistingId = 10000;
        updateDto.setId(notExistingId);

        //when
        Exception exception = assertThrows(TagNotFoundException.class,
                () -> tagService.update(updateDto));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void deleteById_whenNotFound(){
        //given
        Integer notExistingId = 100;

        //when
        Exception exception = Assertions.assertThrows(TagNotFoundException.class,
                () -> tagService.deleteById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }
}

