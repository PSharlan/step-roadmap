package by.itstep.steproadmap.serviceTests;

import by.itstep.steproadmap.dto.category.RoadCategoryCreateDto;
import by.itstep.steproadmap.dto.category.RoadCategoryFullDto;
import by.itstep.steproadmap.dto.category.RoadCategoryUpdateDto;
import by.itstep.steproadmap.entity.RoadCategoryEntity;
import by.itstep.steproadmap.exceptions.RoadCategoryNotFoundException;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.service.RoadCategoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateRoadCategoryCreateDto;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateRoadCategoryUpdateDto;
import static by.itstep.steproadmap.util.EntityGenerationUtils.generateRoadCategory;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class RoadCategoryServiceTest {
    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @Autowired
    private RoadCategoryService roadCategoryService;

    @Autowired
    private RoadRepository roadRepository;

    @BeforeEach
    void setUp() {
        roadRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());

        //when
        RoadCategoryFullDto foundRoadCategory = roadCategoryService.findById(savedRoadCategory.getId());

        //then
        Assertions.assertNotNull(foundRoadCategory);
        Assertions.assertEquals(savedRoadCategory.getId(), foundRoadCategory.getId());
        Assertions.assertEquals(savedRoadCategory.getName(), foundRoadCategory.getName());
    }

    @Test
    void findAll_happyPath() {
        //given
        roadCategoryRepository.save(generateRoadCategory());
        roadCategoryRepository.save(generateRoadCategory());

        //when
        List<RoadCategoryFullDto> foundRoadCategory = roadCategoryService.findAll();

        //then
        Assertions.assertNotNull(foundRoadCategory);
        Assertions.assertEquals(2, foundRoadCategory.size());
    }

    @Test
    void create_happyPath() {
        //given
        RoadCategoryCreateDto roadCategoryToSave = generateRoadCategoryCreateDto();

        //when
        RoadCategoryFullDto savedRoadCategory = roadCategoryService.create(roadCategoryToSave);

        //then
        Assertions.assertNotNull(savedRoadCategory);
        Assertions.assertNotNull(savedRoadCategory.getId());
        Assertions.assertEquals(roadCategoryToSave.getName(),savedRoadCategory.getName());
    }

    @Test
    void update_happyPath(){
        //given
        RoadCategoryCreateDto roadCategoryToSave = generateRoadCategoryCreateDto();

        RoadCategoryFullDto savedRoadCategory = roadCategoryService.create(roadCategoryToSave);

        RoadCategoryUpdateDto updateDto = generateRoadCategoryUpdateDto();

        updateDto.setId(savedRoadCategory.getId());
        updateDto.setName(savedRoadCategory.getName());

        //when
        RoadCategoryFullDto updated = roadCategoryService.update(updateDto);

        //then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals("Backend", updated.getName());
        Assertions.assertEquals(updated.getId(),savedRoadCategory.getId());
    }

    @Test
    void deleteById_happyPath() {
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());

        Integer idForDelete = savedRoadCategory.getId();
        //when
        roadCategoryService.deleteById(idForDelete);

        Exception exception = assertThrows(RoadCategoryNotFoundException.class,
                () -> roadCategoryService.findById(idForDelete));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(idForDelete)));
    }

    @Test
    void findById_whenNotFound() {
        //given
        Integer notExistingId = 100000;

        //when
        Exception exception = assertThrows(RoadCategoryNotFoundException.class,
                () -> roadCategoryService.findById(notExistingId));

        //then
        Assertions.assertTrue((exception.getMessage().contains(String.valueOf(notExistingId))));
    }

    @Test
    void delete_whenNotFound() {
        //given
        Integer notExistingId = 100000;

        //when
        Exception exception = assertThrows(RoadCategoryNotFoundException.class,
                () -> roadCategoryService.deleteById(notExistingId));

        //then
        Assertions.assertTrue((exception.getMessage().contains(String.valueOf(notExistingId))));
    }
}
