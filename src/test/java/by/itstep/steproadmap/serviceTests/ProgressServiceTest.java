package by.itstep.steproadmap.serviceTests;

import by.itstep.steproadmap.dto.progress.ProgressCreateDto;
import by.itstep.steproadmap.dto.progress.ProgressFullDto;
import by.itstep.steproadmap.dto.progress.ProgressUpdateDto;
import by.itstep.steproadmap.entity.ProgressEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.exceptions.EntityNotFoundException;
import by.itstep.steproadmap.exceptions.ProgressNotFoundException;
import by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils;
import by.itstep.steproadmap.repository.ProgressRepository;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import by.itstep.steproadmap.service.ProgressService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Arrays;
import java.util.List;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateProgressUpdateDto;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ProgressServiceTest {

    @Autowired
    private ProgressRepository progressRepository;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private ProgressService progressService;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        progressRepository.deleteAll();
        roadRepository.deleteAll();
        stepRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        ProgressEntity progressToSave = generateProgress();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToSave.setCurrentStep(savedStep);
        progressToSave.setRoad(savedRoad);

        ProgressEntity saved = progressRepository.save(progressToSave);

        //when
        ProgressFullDto foundProgress = progressService.findById(saved.getId());

        //then
        Assertions.assertNotNull(foundProgress);
        Assertions.assertEquals(progressToSave.getId(), foundProgress.getId());
    }

    @Test
    void findByIdWithCompletenessPercent_happyPath() {
        //given
        ProgressEntity progressToSave = generateProgress();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToSave.setCurrentStep(savedStep);
        progressToSave.setRoad(savedRoad);

        ProgressEntity saved = progressRepository.save(progressToSave);

        //when
        ProgressFullDto foundProgress = progressService.findById(saved.getId());

        //then
        Assertions.assertNotNull(foundProgress);
        Assertions.assertEquals(progressToSave.getId(), foundProgress.getId());
        Assertions.assertNotNull(foundProgress.getCompletenessPercent());
        Assertions.assertEquals(100,foundProgress.getCompletenessPercent());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<ProgressEntity> toSave = generateProgressList();

        List<ProgressEntity> saved = progressRepository.saveAll(toSave);

        //when
        List<ProgressFullDto> foundProgresses = progressService.findAll();

        //then
        Assertions.assertNotNull(foundProgresses);
        Assertions.assertEquals(toSave.size(), foundProgresses.size());
        for (int i = 0; i < foundProgresses.size(); i++) {
            Assertions.assertEquals(foundProgresses.get(i).getId(), toSave.get(i).getId());
            Assertions.assertEquals(foundProgresses.get(i).getUserId(), saved.get(i).getUserId());
        }
    }

    @Test
    void findAllProgressesById(){
        //given
        List<ProgressEntity> toSave = generateProgressList();

        List<ProgressEntity> saved = progressRepository.saveAll(toSave);

        //when
        List<ProgressFullDto> foundProgresses = progressService.findAllProgressesByUserId(saved.get(0).getUserId());

        //then
        Assertions.assertNotNull(foundProgresses);
        Assertions.assertEquals(1, foundProgresses.size());
        for (int i = 0; i < foundProgresses.size(); i++) {
            Assertions.assertEquals(foundProgresses.get(i).getRoad().getId(), toSave.get(i).getRoad().getId());
            Assertions.assertEquals(foundProgresses.get(i).getUserId(), saved.get(i).getUserId());
        }
    }

    @Test
    void findAllProgressesByIdWithCompletenessPercent() {
        //given
        List<ProgressEntity> toSave = generateProgressList();

        List<ProgressEntity> saved = progressRepository.saveAll(toSave);

        //when
        List<ProgressFullDto> foundProgresses = progressService.findAllProgressesByUserId(saved.get(0).getUserId());

        //then
        Assertions.assertNotNull(foundProgresses);
        Assertions.assertNotNull(foundProgresses.get(0).getCompletenessPercent());
        for (int i = 0; i < foundProgresses.size(); i++) {
            Assertions.assertEquals(foundProgresses.get(i).getRoad().getId(), toSave.get(i).getRoad().getId());
            Assertions.assertEquals(foundProgresses.get(i).getUserId(), saved.get(i).getUserId());
        }
        Assertions.assertEquals(100,foundProgresses.get(0).getCompletenessPercent());
    }

    @Test
    void create_happyPath() {
        //given
        ProgressCreateDto progressToSave = EntityMapperGenerationUtils.generateProgressCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);
        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToSave.setRoadId(savedRoad.getId());
        progressToSave.setStepId(savedStep.getId());

        //when
        ProgressFullDto saved = progressService.create(progressToSave);
        ProgressFullDto foundSavedProgress = progressService.findById(saved.getId());

        //then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(foundSavedProgress);
        Assertions.assertEquals(saved.getId(), foundSavedProgress.getId());
    }

    @Test
    void update_happyPath() {
        //given
        ProgressCreateDto progressToCreate = EntityMapperGenerationUtils.generateProgressCreateDto();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToCreate.setStepId(savedStep.getId());
        progressToCreate.setRoadId(savedRoad.getId());

        ProgressFullDto savedProgress = progressService.create(progressToCreate);

        ProgressUpdateDto updateDto = generateProgressUpdateDto();
        updateDto.setId(savedProgress.getId());
        updateDto.setCurrentStepId(savedProgress.getCurrentStep().getId());

        //when
        ProgressFullDto updated = progressService.update(updateDto);

        //then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(savedProgress.getId(), updated.getId());
        Assertions.assertEquals(savedProgress.getUserId(), updated.getUserId());
    }

    @Test
    void updateWithPercentage() {
        //given
        ProgressCreateDto progressToCreate = EntityMapperGenerationUtils.generateProgressCreateDto();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToCreate.setStepId(savedStep.getId());
        progressToCreate.setRoadId(savedRoad.getId());

        ProgressFullDto savedProgress = progressService.create(progressToCreate);

        ProgressUpdateDto updateDto = generateProgressUpdateDto();
        updateDto.setId(savedProgress.getId());
        updateDto.setCurrentStepId(savedProgress.getCurrentStep().getId());

        //when
        ProgressFullDto updated = progressService.update(updateDto);

        //then
        Assertions.assertNotNull(updated);
        Assertions.assertNotNull(updated.getCompletenessPercent());
        Assertions.assertEquals(savedProgress.getId(), updated.getId());
        Assertions.assertEquals(savedProgress.getUserId(), updated.getUserId());
        Assertions.assertEquals(100, updated.getCompletenessPercent());
    }

    @Test
    void deleteById_happyPath() {
        //given
        ProgressEntity progressToSave = generateProgress();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        StepEntity stepToSave = generateStep();

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        progressToSave.setCurrentStep(savedStep);
        progressToSave.setRoad(savedRoad);

        ProgressEntity saved = progressRepository.save(progressToSave);
        Integer idForDelete = saved.getId();

        //when
        progressService.deleteById(idForDelete);

        Exception exception = assertThrows(ProgressNotFoundException.class,
                ()-> progressService.findById(saved.getId()));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(saved.getId())));
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistingId = 10000;

        //when
        Exception exception = assertThrows(ProgressNotFoundException.class,
                () -> progressService.findById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound(){
        //given
        ProgressUpdateDto updateDto = generateProgressUpdateDto();

        Integer notExisting = 100000;
        updateDto.setId(notExisting);

        //when
        Exception exception = assertThrows(ProgressNotFoundException.class,
                () -> progressService.update(updateDto));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
    }

    @Test
    void deleteById_whenNotFound(){
        //given
        Integer notExistingId = 10000;

        //when
        Exception exception = assertThrows(EntityNotFoundException.class,
                ()->progressService.deleteById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }


    public List<ProgressEntity> generateProgressList() {
        ProgressEntity progress1 = generateProgress();
        ProgressEntity progress2 = generateProgress();

        RoadEntity roadToSave1 = generateRoad();
        roadToSave1.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad1 = roadRepository.save(roadToSave1);

        RoadEntity roadToSave2 = generateRoad();
        roadToSave2.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad2 = roadRepository.save(roadToSave2);

        StepEntity stepToSet1 = generateStep();
        StepEntity stepToSet2 = generateStep();
        stepToSet1.setRoad(savedRoad1);
        stepToSet2.setRoad(savedRoad2);

        StepEntity savedStep1 = stepRepository.save(stepToSet1);
        StepEntity savedStep2 = stepRepository.save(stepToSet2);

        progress1.setRoad(savedRoad1);
        progress1.setCurrentStep(savedStep1);

        progress2.setRoad(savedRoad2);
        progress2.setCurrentStep(savedStep2);

        return Arrays.asList(progress1, progress2);
    }
}
