package by.itstep.steproadmap.serviceTests;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.step.StepCreateDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.dto.step.StepUpdateDto;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.exceptions.StepNotFoundException;
import by.itstep.steproadmap.repository.ProgressRepository;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import by.itstep.steproadmap.service.StepService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;
import static by.itstep.steproadmap.util.EntityGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class StepServiceTest {

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private StepService stepService;

    @Autowired
    private ProgressRepository progressRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        progressRepository.deleteAll();
        roadRepository.deleteAll();
        stepRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);

        //when
        StepFullDto foundStep = stepService.findById(savedStep.getId());

        //then
        Assertions.assertNotNull(foundStep);
        Assertions.assertEquals(foundStep.getId(), savedStep.getId());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<StepEntity> toSave = generateStepList();

        for (StepEntity entity : toSave) {

            RoadEntity roadToSave = generateRoad();
            roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
            RoadEntity savedRoad = roadRepository.save(roadToSave);

            entity.setRoad(savedRoad);
        }

        List<StepEntity> saved = stepRepository.saveAll(toSave);

        //when
        List<StepFullDto> foundSteps = stepService.findAll();

        //then
        Assertions.assertNotNull(foundSteps);
        Assertions.assertEquals(saved.size(), foundSteps.size());
    }

    @Test
    void create_happyPath() {
        //given
        StepCreateDto stepToSave = generateStepCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        List<ResourceCreateDto> resourceCreateDtoList = generateResourceCreateDtoList();

        stepToSave.setResources(resourceCreateDtoList);
        stepToSave.setRoadId(savedRoad.getId());

        //when
        StepFullDto savedStep = stepService.create(stepToSave);
        StepFullDto foundStep = stepService.findById(savedStep.getId());

        //then
        Assertions.assertNotNull(savedStep);
        Assertions.assertNotNull(savedStep.getId());
        Assertions.assertNotNull(foundStep);
        Assertions.assertEquals(savedStep.getId(), foundStep.getId());
        Assertions.assertNotNull(savedStep.getResources());
        Assertions.assertEquals(savedStep.getResources().size(), resourceCreateDtoList.size());
        for (int i = 0; i < foundStep.getResources().size(); i++) {
            Assertions.assertEquals(foundStep.getResources().get(i).getResourceType(), resourceCreateDtoList.get(i).getResourceType());
            Assertions.assertEquals(foundStep.getResources().get(i).getResourceUrl(), resourceCreateDtoList.get(i).getResourceUrl());
        }
    }

    @Test
    void update_happyPath() {
        //given
        StepCreateDto stepToCreate = generateStepCreateDto();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        List<ResourceCreateDto> resourceCreateDtoList = generateResourceCreateDtoList();

        stepToCreate.setResources(resourceCreateDtoList);
        stepToCreate.setRoadId(savedRoad.getId());

        StepFullDto savedStep = stepService.create(stepToCreate);

        StepUpdateDto updateDto = generateStepUpdateDto();
        updateDto.setId(savedStep.getId());

        //when
        StepFullDto updated = stepService.update(updateDto);

        //then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(savedStep.getId(), updated.getId());
        Assertions.assertEquals(savedStep.getRoadId(), updated.getRoadId());
    }

    @Test
    void deleteById_happyPath() {
        //given
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);

        StepEntity savedStep = stepRepository.save(stepToSave);
        Integer idForDelete = savedStep.getId();

        //when
        stepService.deleteById(idForDelete);

        Exception exception = assertThrows(StepNotFoundException.class,
                () -> stepService.findById(idForDelete) );

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(idForDelete)));
    }

    @Test
    void findById_whenNotFound (){
        //given
        Integer notExistingId  = 10000;

        //when
        Exception exception = assertThrows(StepNotFoundException.class,
                () -> stepService.findById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound(){
        //given
        StepUpdateDto updateDto = generateStepUpdateDto();

        Integer notExistingId = 10000;
        updateDto.setId(notExistingId);

        //when
        Exception exception = assertThrows(StepNotFoundException.class,
                () -> stepService.update(updateDto));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void deleteById_whenNotFound(){
        //given
        Integer notExistingId = 1000000;

        //when
        Exception exception = assertThrows(StepNotFoundException.class,
                () -> stepService.deleteById(notExistingId));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }
}
