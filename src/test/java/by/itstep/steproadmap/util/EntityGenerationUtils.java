package by.itstep.steproadmap.util;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.entity.*;
import by.itstep.steproadmap.entity.enums.ResourceType;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionStage;

public class EntityGenerationUtils {

    public static ProgressEntity generateProgress() {
        ProgressEntity progress = new ProgressEntity();
        progress.setLastActiveTime(Instant.now());
        progress.setUserId((int)(Math.random() * 100));
        return progress;
    }

    public static RoadEntity generateRoad() {
        RoadEntity road = RoadEntity.builder()
                .title("Java tests" + Math.random() * 100)
                .description("Java theory")
                .build();
        return road;
    }

    public static StepEntity generateStep() {
        StepEntity step = StepEntity.builder()
                .title("Java step"+ Math.random()*100)
                .description("grhtht"+Math.random()*100)
                .position(1)
                .build();
        return step;
    }

    public static ResourceEntity generateResource() {

        ResourceEntity resource = ResourceEntity.builder()
                .resourceType(ResourceType.BOOK)
                .resourceUrl("www.frfr")
                .build();
        return resource;
    }

    public static TagEntity generateTag() {
        TagEntity tag = TagEntity.builder()
                .name("Java" + Math.random() * 100)
                .build();
        return tag;
    }

    public static List<RoadEntity> generateRoadList() {
        RoadEntity road1 = RoadEntity.builder()
                .title("Multithread " + 1)
                .description("Some step1")
                .build();

        RoadEntity road2 = RoadEntity.builder()
                .title("Multithread " + 2)
                .description("Some step2")
                .build();

        return new ArrayList<>(Arrays.asList(road1, road2));
    }

    public static List<StepEntity> generateStepList() {
        StepEntity jUnitStep = StepEntity.builder()
                .title("JUnit")
                .description("JUnit tests")
                .position(1)
                .build();

        StepEntity jUnitPractice = StepEntity.builder()
                .title("JUnit 2")
                .description("JUnit practice")
                .position(5)
                .build();
        return Arrays.asList(jUnitStep, jUnitPractice);
    }

    public static List<TagEntity> generateTags(){
        TagEntity javaTag = new TagEntity();
        javaTag.setName("Java");

        TagEntity springTag = new TagEntity();
        springTag.setName("Spring");

        TagEntity hibernateTag = new TagEntity();
        hibernateTag.setName("Hibernate");

        return Arrays.asList(javaTag, springTag, hibernateTag);
    }

    public static RoadCategoryEntity generateRoadCategory() {
        RoadCategoryEntity roadCategory = new RoadCategoryEntity();
        roadCategory.setName("Backend" + Math.random() * 100);
        roadCategory.setRoads(generateRoadList());

        return roadCategory;
    }
}
