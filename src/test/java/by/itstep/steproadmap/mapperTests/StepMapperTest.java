package by.itstep.steproadmap.mapperTests;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import by.itstep.steproadmap.dto.step.StepCreateDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.entity.ResourceEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

import static by.itstep.steproadmap.mapper.StepMapper.STEP_MAPPER;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;

@SpringBootTest
public class StepMapperTest {

    @Test
    void testMapToFullDto(){
        //given
        StepEntity stepEntity = generateStepWithId();
        ResourceEntity resourceEntity = generateResourceWithId();
        RoadEntity roadEntity = generateRoadWithId();

        stepEntity.setRoad(roadEntity);
        stepEntity.setResources(Collections.singletonList(resourceEntity));
        resourceEntity.setStep(stepEntity);


        //when
        StepFullDto fullDto = STEP_MAPPER.mapToFullDto(stepEntity);

        //then
        Assertions.assertEquals(stepEntity.getId(), fullDto.getId());
        Assertions.assertEquals(stepEntity.getTitle(), fullDto.getTitle());
        Assertions.assertEquals(stepEntity.getDescription(), fullDto.getDescription());
        Assertions.assertEquals(stepEntity.getPosition(), fullDto.getPosition());
        Assertions.assertEquals(stepEntity.getRoad().getId(), fullDto.getRoadId());
        Assertions.assertEquals(stepEntity.getResources().size(), fullDto.getResources().size());
        for(int i = 0; i < fullDto.getResources().size(); ++i){
            ResourceEntity entity = stepEntity.getResources().get(i);
            ResourceFullDto dto = fullDto.getResources().get(i);
            Assertions.assertEquals(entity.getId(), dto.getId());
            Assertions.assertEquals(entity.getResourceUrl(), dto.getResourceUrl());
            Assertions.assertEquals(entity.getResourceType(), dto.getResourceType());
            Assertions.assertEquals(entity.getStep().getId(), dto.getStepId());
        }
    }

    @Test
    void testMapToPreviewDto(){
        //given
        StepEntity stepEntity = generateStepWithId();

        //when
        StepFullDto dto = STEP_MAPPER.mapToFullDto(stepEntity);

        //then
        Assertions.assertEquals(stepEntity.getId(), dto.getId());
        Assertions.assertEquals(stepEntity.getTitle(), dto.getTitle());
        Assertions.assertEquals(stepEntity.getDescription(), dto.getDescription());
        Assertions.assertEquals(stepEntity.getPosition(), dto.getPosition());
    }

    @Test
    void testMapToEntity(){
        //given
        StepCreateDto stepCreateDto = generateStepCreateDto();
        RoadEntity road = generateRoadWithId();
        ResourceCreateDto resourceCreateDto = generateResourceCreateDto();

        stepCreateDto.setResources(Collections.singletonList(resourceCreateDto));

        //when
        StepEntity stepEntity = STEP_MAPPER.mapToEntity(stepCreateDto);

        //then
        Assertions.assertNull(stepEntity.getId());
        Assertions.assertNull(stepEntity.getRoad());
        Assertions.assertEquals(stepCreateDto.getTitle(), stepEntity.getTitle());
        Assertions.assertEquals(stepCreateDto.getDescription(), stepEntity.getDescription());
        Assertions.assertEquals(stepCreateDto.getPosition(), stepEntity.getPosition());
        Assertions.assertEquals(stepCreateDto.getResources().size(), stepEntity.getResources().size());
        for(int i = 0; i < stepEntity.getResources().size(); ++i){
            ResourceCreateDto dto = stepCreateDto.getResources().get(i);
            ResourceEntity entity = stepEntity.getResources().get(i);
            Assertions.assertEquals(dto.getResourceType(), entity.getResourceType());
            Assertions.assertEquals(dto.getResourceUrl(), entity.getResourceUrl());
            Assertions.assertNull(entity.getId());
        }
    }

}
