package by.itstep.steproadmap.mapperTests;

import by.itstep.steproadmap.dto.category.RoadCategoryCreateDto;
import by.itstep.steproadmap.dto.category.RoadCategoryFullDto;
import by.itstep.steproadmap.entity.RoadCategoryEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static by.itstep.steproadmap.mapper.RoadCategoryMapper.ROAD_CATEGORY_MAPPER;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;

@SpringBootTest
public class RoadCategoryMapperTest {

    @Test
    void testMapToFullDto(){
        //given
        RoadCategoryEntity roadCategory = generateRoadCategoryWithId();
        roadCategory.setRoads(generateRoadListWithId());

        //when
        RoadCategoryFullDto fullDto = ROAD_CATEGORY_MAPPER.mapToFullDto(roadCategory);

        //then
        Assertions.assertNotNull(fullDto);
        Assertions.assertEquals(roadCategory.getName(),fullDto.getName());
        Assertions.assertEquals(roadCategory.getId(),fullDto.getId());
        Assertions.assertNotNull(fullDto.getRoads());
        Assertions.assertEquals(2,fullDto.getRoads().size());
    }

    @Test
    void mapToEntity() {
        //given
        RoadCategoryCreateDto createDto = generateRoadCategoryCreateDto();

        //when
        RoadCategoryEntity entity = ROAD_CATEGORY_MAPPER.mapToEntity(createDto);

        //then
        Assertions.assertNotNull(entity);
        Assertions.assertNotNull(entity.getName());
        Assertions.assertEquals(createDto.getName(),entity.getName());
    }
}
