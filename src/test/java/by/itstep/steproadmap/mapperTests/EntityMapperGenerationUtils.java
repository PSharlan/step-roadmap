package by.itstep.steproadmap.mapperTests;

import by.itstep.steproadmap.dto.category.RoadCategoryCreateDto;
import by.itstep.steproadmap.dto.category.RoadCategoryUpdateDto;
import by.itstep.steproadmap.dto.progress.ProgressCreateDto;
import by.itstep.steproadmap.dto.progress.ProgressUpdateDto;
import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceUpdateDto;
import by.itstep.steproadmap.dto.road.RoadCreateDto;
import by.itstep.steproadmap.dto.road.RoadUpdateDto;
import by.itstep.steproadmap.dto.step.StepCreateDto;
import by.itstep.steproadmap.dto.step.StepUpdateDto;
import by.itstep.steproadmap.dto.tag.TagUpdateDto;
import by.itstep.steproadmap.entity.*;
import by.itstep.steproadmap.entity.enums.ResourceType;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static by.itstep.steproadmap.util.EntityGenerationUtils.generateRoadList;

public class EntityMapperGenerationUtils {

    public static TagEntity generateTagWithId(){
        return TagEntity.builder().id(1).name("JAVA").build();
    }

    public static by.itstep.steproadmap.dto.tag.TagCreateDto generateTagCreateDto(){
        by.itstep.steproadmap.dto.tag.TagCreateDto dto = new by.itstep.steproadmap.dto.tag.TagCreateDto();
        dto.setName("java");
        return dto;
    }

    public static TagUpdateDto generateTagUpdateDto() {
        TagUpdateDto tagUpdateDto = new TagUpdateDto();
        tagUpdateDto.setName("Java"+((int)Math.random()*100));
        return tagUpdateDto;
    }

    public static ResourceEntity generateResourceWithId(){
        ResourceEntity resourceEntity = new ResourceEntity();
        resourceEntity.setId((int)(Math.random() * 100));
        resourceEntity.setResourceType(ResourceType.VIDEO);
        resourceEntity.setResourceUrl("http://video.mp4");
        return resourceEntity;
    }

    public static ResourceCreateDto generateResourceCreateDto(){
        ResourceCreateDto dto = new ResourceCreateDto();
        dto.setResourceUrl("http://video.mp4");
        dto.setResourceType(ResourceType.VIDEO);
        return dto;
    }

    public  static ResourceUpdateDto generateResourceUpdateDto(){
        ResourceUpdateDto resourceUpdateDto = new ResourceUpdateDto();
        resourceUpdateDto.setResourceType(ResourceType.BOOK);
        resourceUpdateDto.setResourceUrl("http://book.pdf");
        return resourceUpdateDto;
    }

    public static List<ResourceCreateDto> generateResourceCreateDtoList(){
        ResourceCreateDto resourceCreateDto1 = new ResourceCreateDto();
        resourceCreateDto1.setResourceType(ResourceType.BOOK);
        resourceCreateDto1.setResourceUrl("http://book.pdf");

        ResourceCreateDto resourceCreateDto2 = new ResourceCreateDto();
        resourceCreateDto2.setResourceType(ResourceType.BOOK);
        resourceCreateDto2.setResourceUrl("http://book.pdf");

        return Arrays.asList(resourceCreateDto1,resourceCreateDto2);
    }

    public static StepEntity generateStepWithId(){
        StepEntity stepEntity = new StepEntity();
        stepEntity.setId((int)(Math.random() * 100));
        stepEntity.setTitle("title");
        stepEntity.setDescription("disc");
        stepEntity.setPosition(1);
        return stepEntity;
    }

    public static RoadEntity generateRoadWithId(){
        RoadEntity roadEntity = new RoadEntity();
        roadEntity.setId((int)(Math.random() * 100));
        roadEntity.setTitle("title");
        roadEntity.setDescription("description");
        return roadEntity;
    }

    public static StepCreateDto generateStepCreateDto(){
        StepCreateDto createDto = new StepCreateDto();
        createDto.setTitle("title");
        createDto.setDescription("desc");
        createDto.setPosition(1);
        return createDto;
    }

    public static StepUpdateDto generateStepUpdateDto(){
        StepUpdateDto stepUpdateDto = new StepUpdateDto();
        stepUpdateDto.setTitle("Step");
        stepUpdateDto.setDescription("Some description");
        stepUpdateDto.setPosition(3);
        return stepUpdateDto;
    }

    public static RoadCreateDto generateRoadCreateDto(){
        RoadCreateDto roadCreateDto = new RoadCreateDto();
        roadCreateDto.setTitle("title");
        roadCreateDto.setDescription("desc");
        return roadCreateDto;
    }

    public static RoadUpdateDto generateRoadUpdateDto(){
        RoadUpdateDto roadUpdateDto = new RoadUpdateDto();
        roadUpdateDto.setTitle("java vs python");
        roadUpdateDto.setDescription("Learn more");
        return roadUpdateDto;
    }

    public static ProgressCreateDto generateProgressCreateDto(){
        ProgressCreateDto dto = new ProgressCreateDto();
        dto.setUserId((int)(Math.random() * 100));
        dto.setUserId((int)(Math.random() * 10));
        dto.setStepId((int)(Math.random() * 10));
        return dto;
    }

    public static ProgressUpdateDto generateProgressUpdateDto(){
        ProgressUpdateDto dto = new ProgressUpdateDto();
        dto.setCurrentStepId((int)(Math.random() * 100));
        return dto;
    }

    public static ProgressEntity generateProgressWithId(){
        ProgressEntity progressEntity = new ProgressEntity();
        progressEntity.setId((int)(Math.random() * 100));
        progressEntity.setUserId((int)(Math.random() * 100));
        progressEntity.setLastActiveTime(Instant.now());
        return progressEntity;
    }

    public static RoadCategoryCreateDto generateRoadCategoryCreateDto(){
        RoadCategoryCreateDto dto= new RoadCategoryCreateDto();
        dto.setName("Backend");
        return dto;
    }

    public static RoadCategoryUpdateDto generateRoadCategoryUpdateDto(){
        RoadCategoryUpdateDto dto = new RoadCategoryUpdateDto();
        dto.setName("Frontend");
        return dto;
    }

    public static RoadCategoryEntity generateRoadCategoryWithId(){
        RoadCategoryEntity roadCategory = new RoadCategoryEntity();
        roadCategory.setId((int)(Math.random() * 100));
        roadCategory.setName("Database");
        roadCategory.setRoads(generateRoadList());
        return roadCategory;
    }

    public static List<RoadEntity> generateRoadListWithId(){
       return Arrays.asList(generateRoadWithId(),generateRoadWithId());
    }
}
