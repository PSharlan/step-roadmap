package by.itstep.steproadmap.mapperTests;

import by.itstep.steproadmap.dto.progress.ProgressCreateDto;
import by.itstep.steproadmap.dto.progress.ProgressFullDto;
import by.itstep.steproadmap.entity.ProgressEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static by.itstep.steproadmap.mapper.ProgressMapper.PROGRESS_MAPPER;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;

@SpringBootTest
public class ProgressMapperTest {

    @Test
    void testMapToFullDto(){
        //given
        ProgressEntity progressEntity = generateProgressWithId();
        RoadEntity roadEntity = generateRoadWithId();
        StepEntity stepEntity = generateStepWithId();

        progressEntity.setCurrentStep(stepEntity);
        progressEntity.setRoad(roadEntity);

        //when
        ProgressFullDto progressFullDto = PROGRESS_MAPPER.mapToFullDto(progressEntity);

        //then
        Assertions.assertEquals(progressEntity.getId(), progressFullDto.getId());
        Assertions.assertEquals(progressEntity.getUserId(), progressFullDto.getUserId());
        Assertions.assertEquals(progressEntity.getLastActiveTime(), progressFullDto.getLastActiveTime());

        Assertions.assertEquals(stepEntity.getId(), progressFullDto.getCurrentStep().getId());
        Assertions.assertEquals(stepEntity.getPosition(), progressFullDto.getCurrentStep().getPosition());
        Assertions.assertEquals(stepEntity.getTitle(), progressFullDto.getCurrentStep().getTitle());
        Assertions.assertEquals(stepEntity.getDescription(), progressFullDto.getCurrentStep().getDescription());

        Assertions.assertEquals(roadEntity.getId(), progressFullDto.getRoad().getId());
        Assertions.assertEquals(roadEntity.getTitle(), progressFullDto.getRoad().getTitle());
        Assertions.assertEquals(roadEntity.getDescription(), progressFullDto.getRoad().getDescription());
    }

    @Test
    void mapToEntity(){
        //given
        ProgressCreateDto dto = generateProgressCreateDto();

        //when
        ProgressEntity progressEntity = PROGRESS_MAPPER.mapToEntity(dto);

        //then
        Assertions.assertEquals(dto.getUserId(), progressEntity.getUserId());
        Assertions.assertNull(progressEntity.getId());
        Assertions.assertNull(progressEntity.getCurrentStep());
        Assertions.assertNull(progressEntity.getRoad());
        Assertions.assertNull(progressEntity.getLastActiveTime());
    }

}
