package by.itstep.steproadmap.mapperTests;

import by.itstep.steproadmap.dto.road.RoadCreateDto;
import by.itstep.steproadmap.dto.road.RoadFullDto;
import by.itstep.steproadmap.dto.road.RoadPreviewDto;
import by.itstep.steproadmap.dto.step.StepFullDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.entity.TagEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

import static by.itstep.steproadmap.mapper.RoadMapper.ROAD_MAPPER;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;

@SpringBootTest
public class RoadMapperTest {

    @Test
    void testMapToFullDto(){
        //given
        RoadEntity roadEntity = generateRoadWithId();
        TagEntity tagEntity = generateTagWithId();
        StepEntity stepEntity = generateStepWithId();

        roadEntity.setTags(Collections.singletonList(tagEntity));
        roadEntity.setSteps(Collections.singletonList(stepEntity));

        //when
        RoadFullDto roadFullDto = ROAD_MAPPER.mapToFullDto(roadEntity);

        //then
        Assertions.assertEquals(roadEntity.getId(), roadFullDto.getId());
        Assertions.assertEquals(roadEntity.getDescription(), roadFullDto.getDescription());
        Assertions.assertEquals(roadEntity.getTitle(), roadFullDto.getTitle());
        Assertions.assertEquals(roadEntity.getSteps().size(), roadFullDto.getSteps().size());
        Assertions.assertEquals((roadEntity.getTags().size()), roadFullDto.getSteps().size());

        for(int i = 0; i < roadEntity.getTags().size(); ++i){
            TagPreviewDto dto = roadFullDto.getTags().get(i);
            TagEntity entity = roadEntity.getTags().get(i);
            Assertions.assertEquals(entity.getName(), dto.getName());
            Assertions.assertEquals(entity.getId(), dto.getId());
        }

        for(int i = 0; i < roadEntity.getSteps().size(); ++i){
            StepFullDto dto = roadFullDto.getSteps().get(i);
            StepEntity entity = roadEntity.getSteps().get(i);
            Assertions.assertEquals(entity.getId(), dto.getId());
            Assertions.assertEquals(entity.getPosition(), dto.getPosition());
            Assertions.assertEquals(entity.getDescription(), dto.getDescription());
            Assertions.assertEquals(entity.getTitle(), dto.getTitle());
        }
    }

    @Test
    void testMapToPreviewDto(){
        //given
        RoadEntity roadEntity = generateRoadWithId();

        //when
        RoadPreviewDto roadPreviewDto = ROAD_MAPPER.mapToPreviewDto(roadEntity);

        //then
        Assertions.assertEquals(roadEntity.getId(), roadPreviewDto.getId());
        Assertions.assertEquals(roadEntity.getDescription(), roadPreviewDto.getDescription());
        Assertions.assertEquals(roadEntity.getTitle(), roadPreviewDto.getTitle());
    }

    @Test
    void testMapToEntity(){
        //given
        RoadCreateDto roadCreateDto = generateRoadCreateDto();

        //when
        RoadEntity roadEntity = ROAD_MAPPER.mapToEntity(roadCreateDto);

        //then
        Assertions.assertNull(roadEntity.getId());
        Assertions.assertTrue(roadEntity.getSteps().isEmpty());
        Assertions.assertTrue(roadEntity.getTags().isEmpty());
        Assertions.assertEquals(roadCreateDto.getTitle(), roadEntity.getTitle());
        Assertions.assertEquals(roadCreateDto.getDescription(), roadEntity.getDescription());
    }

}
