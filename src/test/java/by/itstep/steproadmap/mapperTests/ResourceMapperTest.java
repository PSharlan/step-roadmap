package by.itstep.steproadmap.mapperTests;

import by.itstep.steproadmap.dto.resource.ResourceCreateDto;
import by.itstep.steproadmap.dto.resource.ResourceFullDto;
import by.itstep.steproadmap.entity.ResourceEntity;
import by.itstep.steproadmap.entity.StepEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static by.itstep.steproadmap.mapper.ResourceMapper.RESOURCE_MAPPER;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.*;

@SpringBootTest
public class ResourceMapperTest {

    @Test
    void testMapToFullDto(){
        //given
        ResourceEntity resourceEntity = generateResourceWithId();
        StepEntity stepEntity = generateStepWithId();
        resourceEntity.setStep(stepEntity);

        //when
        ResourceFullDto fullDto = RESOURCE_MAPPER.mapToFullDto(resourceEntity);

        //then
        Assertions.assertEquals(resourceEntity.getId(), fullDto.getId());
        Assertions.assertEquals(resourceEntity.getResourceType(), fullDto.getResourceType());
        Assertions.assertEquals(resourceEntity.getResourceUrl(), fullDto.getResourceUrl());
        Assertions.assertEquals(resourceEntity.getStep().getId(), fullDto.getStepId());
    }

    @Test
    void testMapToEntity(){
        //given
        ResourceCreateDto createDto = generateResourceCreateDto();

        //when
        ResourceEntity entity = RESOURCE_MAPPER.mapToEntity(createDto);

        //then
        Assertions.assertEquals(createDto.getResourceUrl(), entity.getResourceUrl());
        Assertions.assertEquals(createDto.getResourceType(), entity.getResourceType());
        Assertions.assertNull(entity.getId());
        Assertions.assertNull(entity.getStep());
    }

}
