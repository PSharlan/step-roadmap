package by.itstep.steproadmap.mapperTests;

import by.itstep.steproadmap.dto.tag.TagCreateDto;
import by.itstep.steproadmap.dto.tag.TagPreviewDto;
import by.itstep.steproadmap.entity.TagEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static by.itstep.steproadmap.mapper.TagMapper.*;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateTagWithId;
import static by.itstep.steproadmap.mapperTests.EntityMapperGenerationUtils.generateTagCreateDto;

@SpringBootTest
public class TagMapperTest {

    @Test
    void testMapToPreviewDto(){
        //given
        TagEntity tag = generateTagWithId();

        //when
        TagPreviewDto dto = TAG_MAPPER.mapToPreviewDto(tag);

        //then
        Assertions.assertEquals(tag.getId(), dto.getId());
        Assertions.assertEquals(tag.getName(), dto.getName());
    }

    @Test
    void testMapToEntity(){
        //given
        TagCreateDto createDto = generateTagCreateDto();

        //when
        TagEntity tagEntity = TAG_MAPPER.mapToEntity(createDto);

        //then
        Assertions.assertNull(tagEntity.getId());
        Assertions.assertTrue(tagEntity.getRoads().isEmpty());
        Assertions.assertEquals(createDto.getName(),tagEntity.getName());
    }
}
