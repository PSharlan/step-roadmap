package by.itstep.steproadmap.repositoryTests;

import by.itstep.steproadmap.entity.ResourceEntity;
import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.steproadmap.util.EntityGenerationUtils.*;

@SpringBootTest
class ResourceRepositoryTest {

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private ProgressRepository progressRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        progressRepository.deleteAll();
        resourceRepository.deleteAll();
        stepRepository.deleteAll();
        roadRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void testCreate_happyPath() {
        //given
        ResourceEntity resourceToSave = generateResource();
        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);

        //when
        ResourceEntity saved = resourceRepository.save(resourceToSave);

        //then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll_happyPath() {
        //given
        ResourceEntity resourceToSave = generateResource();
        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);
        resourceRepository.save(resourceToSave);

        //when
        List<ResourceEntity> found = resourceRepository.findAll();

        //then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(1, found.size());
    }

    @Test
    void testFindOneById_happyPath() {
        //given
        ResourceEntity resourceToSave = generateResource();
        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);
        ResourceEntity saved = resourceRepository.save(resourceToSave);

        //when
        ResourceEntity found = resourceRepository.findOneById(saved.getId());

        //then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        ResourceEntity resourceToSave = generateResource();
        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);
        ResourceEntity savedResource = resourceRepository.save(resourceToSave);

        //when
        ResourceEntity resourceToUpdate = resourceRepository.findOneById(savedResource.getId());

        resourceToUpdate.setResourceUrl("www.oracle.com");
        ResourceEntity updatedResource = resourceRepository.save(resourceToUpdate);

        //then
        Assertions.assertNotNull(resourceToUpdate);
        Assertions.assertNotEquals(updatedResource.getResourceUrl(), savedResource.getResourceUrl());
    }

    @Test
    void testDeleteAll_happyPath() {
        //given
        ResourceEntity resourceToSave = generateResource();
        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);
        resourceRepository.save(resourceToSave);

        //when
        resourceRepository.deleteAll();
        List<ResourceEntity> foundResources = resourceRepository.findAll();

        //then
        Assertions.assertEquals(0, foundResources.size());
    }

    @Test
    void testDeleteById_happyPath() {
        //given
        ResourceEntity resourceToSave = generateResource();
        StepEntity stepToSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        resourceToSave.setStep(savedStep);
        ResourceEntity savedResource = resourceRepository.save(resourceToSave);

        //when
        resourceRepository.deleteById(savedResource.getId());

        //then
        Assertions.assertNull(resourceRepository.findOneById(savedResource.getId()));
    }
}
