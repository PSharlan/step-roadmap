package by.itstep.steproadmap.repositoryTests;

import by.itstep.steproadmap.entity.*;
import by.itstep.steproadmap.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.List;

import static by.itstep.steproadmap.util.EntityGenerationUtils.*;

@SpringBootTest
class ProgressRepositoryTest {

    @Autowired
    private ProgressRepository progressRepository;

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        progressRepository.deleteAll();
        stepRepository.deleteAll();
        roadRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void testCreate_happyPath() {
        //given
        ProgressEntity toSave = generateProgress();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        toSave.setCurrentStep(savedStep);
        toSave.setRoad(savedRoad);

        //when
        ProgressEntity saved = progressRepository.save(toSave);

        //then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll_happyPath() {
        //given
        ProgressEntity toSave = generateProgress();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        toSave.setCurrentStep(savedStep);
        toSave.setRoad(savedRoad);

        progressRepository.save(toSave);

        //when
        List<ProgressEntity> found = progressRepository.findAll();

        //then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(1, found.size());
    }

    @Test
    void testFindAllProgressesById() {
        //given
        ProgressEntity toSave = generateProgress();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        toSave.setCurrentStep(savedStep);
        toSave.setRoad(savedRoad);

        ProgressEntity toSave2 = generateProgress();
        RoadEntity roadToSave2 = generateRoad();

        roadToSave2.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad2 = roadRepository.save(roadToSave2);

        StepEntity stepToSave2 = generateStep();

        stepToSave2.setRoad(savedRoad2);
        StepEntity savedStep2 = stepRepository.save(stepToSave2);

        toSave2.setCurrentStep(savedStep2);
        toSave2.setRoad(savedRoad2);

        progressRepository.save(toSave);
        progressRepository.save(toSave2);

        //when
        List<ProgressEntity> found = progressRepository.findAllProgressesByUserId(toSave.getUserId());
        System.out.println(found);

        //then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(1,found.size());
        Assertions.assertEquals(found.get(0).getUserId(), toSave.getUserId());
    }

    @Test
    void testFindOneById_happyPath() {
        //given
        ProgressEntity toSave = generateProgress();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        toSave.setCurrentStep(savedStep);
        toSave.setRoad(savedRoad);

        ProgressEntity saved = progressRepository.save(toSave);

        //when
        ProgressEntity found = progressRepository.findOneById(saved.getId());

        //then
        Assertions.assertNotNull(saved);
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        ProgressEntity toSave = generateProgress();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        toSave.setCurrentStep(savedStep);
        toSave.setRoad(savedRoad);

        ProgressEntity saved = progressRepository.save(toSave);

        //when
        ProgressEntity progressToUpdate = progressRepository.findOneById(saved.getId());
        progressToUpdate.setLastActiveTime(Instant.now());

        ProgressEntity updated = progressRepository.save(progressToUpdate);

        //when
        Assertions.assertNotEquals(updated.getLastActiveTime(), saved.getLastActiveTime());
    }

    @Test
    void testDeleteAll_happyPath() {
        //given
        ProgressEntity toSave = generateProgress();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        toSave.setCurrentStep(savedStep);
        toSave.setRoad(savedRoad);

        progressRepository.save(toSave);

        //when
        progressRepository.deleteAll();
        List<ProgressEntity> found = progressRepository.findAll();

        //then
        Assertions.assertEquals(0, found.size());
    }

    @Test
    void testDeleteById_happyPath() {
        //given
        ProgressEntity toSave = generateProgress();
        StepEntity stepToSave = generateStep();
        RoadEntity roadToSave = generateRoad();

        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        stepToSave.setRoad(savedRoad);
        StepEntity savedStep = stepRepository.save(stepToSave);

        toSave.setCurrentStep(savedStep);
        toSave.setRoad(savedRoad);

        ProgressEntity saved = progressRepository.save(toSave);

        //when
        progressRepository.deleteById(saved.getId());

        //then
        ProgressEntity deletedEntity = progressRepository.findOneById(saved.getId());
        Assertions.assertNull(deletedEntity);
    }
}
