package by.itstep.steproadmap.repositoryTests;

import by.itstep.steproadmap.entity.RoadCategoryEntity;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.steproadmap.util.EntityGenerationUtils.generateRoadCategory;

@SpringBootTest
public class RoadCategoryRepositoryTests {
    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @Autowired
    private RoadRepository roadRepository;

    @BeforeEach
    void setUp() {
        roadRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void testCreate_happyPath() {
        //given
        RoadCategoryEntity roadCategoryToSave = roadCategoryRepository.save(generateRoadCategory());

        //when
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(roadCategoryToSave);

        //then
        Assertions.assertNotNull(savedRoadCategory);
        Assertions.assertNotNull(savedRoadCategory.getId());
        Assertions.assertNotNull(savedRoadCategory.getName());
    }

    @Test
    void findAll_happyPath() {
        //given
         roadCategoryRepository.save(generateRoadCategory());
         roadCategoryRepository.save(generateRoadCategory());

        //when
        List<RoadCategoryEntity> foundRoadCategory = roadCategoryRepository.findAll();

        //then
        Assertions.assertNotNull(foundRoadCategory);
        Assertions.assertEquals(2, foundRoadCategory.size());
        for (int i = 0; i < foundRoadCategory.size(); i++) {
            Assertions.assertNotNull(foundRoadCategory.get(i).getId());
            Assertions.assertNotNull(foundRoadCategory.get(i).getName());
        }
    }

    @Test
    void findOneById(){
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());

        //when
        RoadCategoryEntity foundRoadCategory = roadCategoryRepository.findOneById(savedRoadCategory.getId());

        //then
        Assertions.assertNotNull(foundRoadCategory);
        Assertions.assertEquals(savedRoadCategory.getId(), foundRoadCategory.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());

        //when
        RoadCategoryEntity roadCategoryToUpdate = roadCategoryRepository.findOneById(savedRoadCategory.getId());

        roadCategoryToUpdate.setName("Mega-Backend");
        RoadCategoryEntity updatedRoadCategory = roadCategoryRepository.save(roadCategoryToUpdate);

        //then
        Assertions.assertNotNull(updatedRoadCategory);
        Assertions.assertEquals("Mega-Backend", updatedRoadCategory.getName());
        Assertions.assertNotEquals(savedRoadCategory.getName(),updatedRoadCategory.getName());
    }

    @Test
    void testDeleteAll_happyPath(){
        //given
        roadCategoryRepository.save(generateRoadCategory());
        roadCategoryRepository.save(generateRoadCategory());

        //when
        roadCategoryRepository.deleteAll();
        List<RoadCategoryEntity> foundRoadCategory = roadCategoryRepository.findAll();

        //then
        Assertions.assertEquals(0,foundRoadCategory.size());
    }

    @Test
    void testDeleteById_happyPath() {
        //given
        RoadCategoryEntity savedRoadCategory = roadCategoryRepository.save(generateRoadCategory());

        //when
        roadCategoryRepository.deleteById(savedRoadCategory.getId());

        //then
        Assertions.assertNull(roadCategoryRepository.findOneById(savedRoadCategory.getId()));


    }

}
