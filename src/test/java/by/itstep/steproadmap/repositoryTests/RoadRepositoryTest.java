package by.itstep.steproadmap.repositoryTests;

import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.entity.TagEntity;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.steproadmap.util.EntityGenerationUtils.*;

@SpringBootTest
public class RoadRepositoryTest {

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        roadRepository.deleteAll();
        tagRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void testCreate_happyPath() {
        //given
        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));

        List<StepEntity> stepList = generateStepList();
        roadToSave.setSteps(stepList);

        for (StepEntity steps : stepList) {
            steps.setRoad(roadToSave);
        }

        //when
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        //then
        Assertions.assertNotNull(savedRoad);
        Assertions.assertNotNull(savedRoad.getId());
    }

    @Test
    void testFindAll_happyPath() {
        //given
        List<RoadEntity> roads = generateRoadList();

        for(RoadEntity road : roads){
            List<StepEntity> steps = generateStepList();
            road.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
            road.setSteps(steps);
            for (StepEntity step : steps) {
                step.setRoad(road);
            }
        }

        List<RoadEntity> savedRoads = roadRepository.saveAll(roads);

        //when
        List<RoadEntity> foundRoads = roadRepository.findAll();

        //then
        Assertions.assertNotNull(foundRoads);
        Assertions.assertEquals(savedRoads.size(), foundRoads.size());
    }

    @Test
    void testFindAllRoadByCategoryId_happyPath() {
        //given
        List<RoadEntity> roads = generateRoadList();

        for(RoadEntity road : roads){
            List<StepEntity> steps = generateStepList();
            road.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
            road.setSteps(steps);
            for (StepEntity step : steps) {
                step.setRoad(road);
            }
        }
        List<RoadEntity> savedRoads = roadRepository.saveAll(roads);

        //when
        List<RoadEntity> foundRoads = roadRepository.findAllRoadsByRoadCategoryId(savedRoads.get(0).getRoadCategory().getId());

        //then
        Assertions.assertNotNull(foundRoads);
        Assertions.assertEquals(1 ,foundRoads.size());
    }

    @Test
    void testFindByTitle_happyPath() {
        //given
        List<RoadEntity> roads = generateRoadList();

        String titleToFound = roads.get(0).getTitle();
        int roadsWithSameTitle = 0;
        for(RoadEntity road : roads){
            if(road.getTitle().equals(titleToFound)){
                ++roadsWithSameTitle;
            }
        }

        for(RoadEntity road : roads){
            List<StepEntity> steps = generateStepList();
            road.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
            road.setSteps(steps);
            for (StepEntity step : steps) {
                step.setRoad(road);
            }
        }
        roadRepository.saveAll(roads);

        //when
        List<RoadEntity> found = roadRepository.findByTitle(titleToFound);

        //then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(roadsWithSameTitle, found.size());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));

        List<StepEntity> stepList = generateStepList();
        roadToSave.setSteps(stepList);
        for (StepEntity steps : stepList) {
            steps.setRoad(roadToSave);
        }

        RoadEntity savedRoad = roadRepository.save(roadToSave);

        String newTitle = "new title";
        //when
        RoadEntity roadToUpdate = roadRepository.findOneById(savedRoad.getId());

        roadToUpdate.setTitle(newTitle);

        RoadEntity updated = roadRepository.save(roadToUpdate);

        //then
        Assertions.assertEquals(updated.getTitle(), newTitle);

    }

    @Test
    void testDeleteAll_happyPath() {
        //given
        List<RoadEntity> roads = generateRoadList();

        for(RoadEntity road : roads){
            List<StepEntity> steps = generateStepList();
            road.setSteps(steps);
            for (StepEntity step : steps) {
                step.setRoad(road);
            }
        }

        //when
        roadRepository.deleteAll();
        List<RoadEntity> found = roadRepository.findAll();

        //then
        Assertions.assertEquals(0, found.size());
    }

    @Test
    void testDeleteById_happyPath() {
        //given
        RoadEntity road = generateRoad();
        road.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));


        List<StepEntity> stepList = generateStepList();
        road.setSteps(stepList);
        for (StepEntity step : stepList) {
            step.setRoad(road);
        }

        RoadEntity savedRoad = roadRepository.save(road);

        //when
        roadRepository.deleteById(savedRoad.getId());

        //then
        Assertions.assertNull(roadRepository.findOneById(savedRoad.getId()));
    }

    @Test
    void testFindByTag_happyPath(){
        //given
        List<TagEntity> tagsToSave = generateTags();
        List<RoadEntity> roadsToSave = generateRoadList();

        List<TagEntity> savedTags = tagRepository.saveAll(tagsToSave);
        for(RoadEntity road : roadsToSave){
            road.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
            road.setTags(savedTags);
        }

        roadRepository.saveAll(roadsToSave);

        //when
        List<RoadEntity> foundRoads = roadRepository.findAllByTag(savedTags.get(0));

        //then
        Assertions.assertEquals(2, foundRoads.size());
        for(RoadEntity foundRoad : foundRoads){
            Assertions.assertTrue(foundRoad.getTags().contains(savedTags.get(0)));
        }

    }

}
