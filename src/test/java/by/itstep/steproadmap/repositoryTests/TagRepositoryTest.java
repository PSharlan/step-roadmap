package by.itstep.steproadmap.repositoryTests;

import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.TagEntity;
import by.itstep.steproadmap.repository.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.steproadmap.util.EntityGenerationUtils.*;

@SpringBootTest
public class TagRepositoryTest {

    @Autowired
    private TagRepository tagRepository;

    @BeforeEach
    void setUp() {
        tagRepository.deleteAll();
    }

    @Test
    void testCreateTag_happyPath() {
        //given
        TagEntity toSave = generateTag();
        List<RoadEntity> roadList = generateRoadList();
        toSave.setRoads(roadList);

        //when
        TagEntity saved = tagRepository.save(toSave);


        //then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(toSave.getId());
    }

    @Test
    void testFindAll_happyPath() {
        //given
        List<TagEntity> tags = generateTags();

        List<RoadEntity> roadList = generateRoadList();

        for(TagEntity tag : tags){
            tag.setRoads(roadList);
        }

        List<TagEntity> savedTags = tagRepository.saveAll(tags);

        //when
        List<TagEntity> found = tagRepository.findAll();

        //then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(savedTags.size(), found.size());
    }

    @Test
    void testFindByName_happyPath() {
        //given
        List<TagEntity> tags = generateTags();

        List<RoadEntity> roadList = generateRoadList();

        for(TagEntity tag : tags){
            tag.setRoads(roadList);
        }

        List<TagEntity> savedTags = tagRepository.saveAll(tags);
        String nameToFind = savedTags.get(0).getName();

        //when
        TagEntity found = tagRepository.findByName(nameToFind);

        //then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(found.getName(), nameToFind);
    }

    @Test
    void testUpdate_happyPath() {
        //given
        List<TagEntity> tags = generateTags();

        List<RoadEntity> roadList = generateRoadList();
        for (TagEntity tag : tags){
            tag.setRoads(roadList);
        }

        List<TagEntity> savedTags = tagRepository.saveAll(tags);
        String newName = "unique";
        TagEntity tagToUpdate = savedTags.get(0);

        //when
        TagEntity foundTag = tagRepository.findOneById(tagToUpdate.getId());
        foundTag.setName(newName);

        TagEntity updated = tagRepository.save(foundTag);

        List<TagEntity> found = tagRepository.findAll();

        //then
        Assertions.assertEquals(updated.getName(), newName);
        Assertions.assertEquals(savedTags.size(), found.size());
    }

    @Test
    void testDeleteAll_happyPath() {
        //given
        List<TagEntity> tags = generateTags();

        List<RoadEntity> roadList = generateRoadList();

        for(TagEntity tag : tags){
            tag.setRoads(roadList);
        }

        List<TagEntity> saved = tagRepository.saveAll(tags);

        //when
        tagRepository.deleteAll();
        List<TagEntity> found = tagRepository.findAll();

        //then
        Assertions.assertEquals(0, found.size());
    }

    @Test
    void testDeleteById_happyPath() {
        //given
        TagEntity tag = generateTag();

        List<RoadEntity> roadList = generateRoadList();
        tag.setRoads(roadList);

        TagEntity saved = tagRepository.save(tag);

        //when
        tagRepository.deleteById(saved.getId());

        //then
        Assertions.assertNull(tagRepository.findOneById(saved.getId()));
    }
}