package by.itstep.steproadmap.repositoryTests;

import by.itstep.steproadmap.entity.RoadEntity;
import by.itstep.steproadmap.entity.StepEntity;
import by.itstep.steproadmap.repository.RoadCategoryRepository;
import by.itstep.steproadmap.repository.RoadRepository;
import by.itstep.steproadmap.repository.StepRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.steproadmap.util.EntityGenerationUtils.*;

@SpringBootTest
class StepRepositoryTest {

    @Autowired
    private StepRepository stepRepository;

    @Autowired
    private RoadRepository roadRepository;

    @Autowired
    private RoadCategoryRepository roadCategoryRepository;

    @BeforeEach
    void setUp() {
        stepRepository.deleteAll();
        roadRepository.deleteAll();
        roadCategoryRepository.deleteAll();
    }

    @Test
    void testCreate_happyPath() {
        //given
        StepEntity toSave = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        toSave.setRoad(savedRoad);

        //when
        StepEntity saved = stepRepository.save(toSave);

        //then
        Assertions.assertNotNull(toSave);
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindAll_happyPath() {
        //given
        List<StepEntity> steps = generateStepList();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        for(StepEntity step : steps){
            step.setRoad(savedRoad);
        }

        List<StepEntity> savedSteps = stepRepository.saveAll(steps);

        //when
        List<StepEntity> found = stepRepository.findAll();

        //then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(savedSteps.size(), found.size());
    }

    @Test
    void testFindByTitle_happyPath() {
        //given
        List<StepEntity> steps = generateStepList();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        for(StepEntity step : steps){
            step.setRoad(savedRoad);
        }

        List<StepEntity> savedSteps = stepRepository.saveAll(steps);
        String titleToFound = savedSteps.get(0).getTitle();
        //when
        List<StepEntity> found = stepRepository.findByTitle(titleToFound);

        //then
        Assertions.assertNotNull(found);
        for(StepEntity step : found){
            Assertions.assertEquals(step.getTitle(), titleToFound);
        }
    }

    @Test
    void testUpdate_happyPath() {
        //given
        StepEntity step = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        step.setRoad(savedRoad);

        stepRepository.save(step);

        String newTitle = "new title";
        //when
        StepEntity stepToUpdate = stepRepository.findOneById(step.getId());

        stepToUpdate.setTitle(newTitle);
        StepEntity updated = stepRepository.save(stepToUpdate);

        StepEntity found = stepRepository.findOneById(updated.getId());

        //then
        Assertions.assertEquals(updated.getTitle(), newTitle);
    }

    @Test
    void testDeleteAll_happyPath() {
        //given
        List<StepEntity> steps = generateStepList();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        for(StepEntity step : steps){
            step.setRoad(savedRoad);
        }

        List<StepEntity> savedSteps = stepRepository.saveAll(steps);

        //when
        stepRepository.deleteAll();
        List<StepEntity> found = stepRepository.findAll();

        //then
        Assertions.assertEquals(0, found.size());
    }

    @Test
    void testDeleteById_happyPath() {
        //given
        StepEntity step = generateStep();

        RoadEntity roadToSave = generateRoad();
        roadToSave.setRoadCategory(roadCategoryRepository.save(generateRoadCategory()));
        RoadEntity savedRoad = roadRepository.save(roadToSave);

        step.setRoad(savedRoad);

        StepEntity saved = stepRepository.save(step);

        //when
        stepRepository.deleteById(step.getId());

        //then
        Assertions.assertNull(stepRepository.findOneById(saved.getId()));
    }
}
